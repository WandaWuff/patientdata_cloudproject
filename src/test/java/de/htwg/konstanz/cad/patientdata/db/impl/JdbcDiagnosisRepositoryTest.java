package de.htwg.konstanz.cad.patientdata.db.impl;

import de.htwg.konstanz.cad.patientdata.api.model.Diagnosis;
import de.htwg.konstanz.cad.patientdata.api.model.ImmutableDiagnosis;
import de.htwg.konstanz.cad.patientdata.config.PatientDataConfiguration;
import de.htwg.konstanz.cad.provisioning.config.IntegrationTestProvisioningConfiguration;
import de.htwg.konstanz.cad.tenant.db.config.TenantDbConfiguration;
import de.htwg.konstanz.cad.util.test.IntegrationTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ExtendWith(SpringExtension.class)
@JdbcTest(excludeAutoConfiguration = LiquibaseAutoConfiguration.class)
@ContextConfiguration(classes = {PatientDataConfiguration.class, IntegrationTestProvisioningConfiguration.class})
@AutoConfigureTestDatabase(replace = NONE)
@TestPropertySource(locations = "classpath:test-application.properties")
@IntegrationTest
class JdbcDiagnosisRepositoryTest {

    @Autowired
    public JdbcDiagnosisRepository sut;

    @Test
    void save() {
        Diagnosis diagnosis = ImmutableDiagnosis.builder()
                .id(UUID.randomUUID())
                .doctor("doctor")
                .date(ZonedDateTime.now())
                .icd("12345")
                .place("Konstanz")
                .build();

        sut.save(diagnosis);
    }

    @Test
    void findAll() {
        Diagnosis diagnosis = ImmutableDiagnosis.builder()
                .id(UUID.randomUUID())
                .doctor("doctor")
                .date(ZonedDateTime.now().withNano(0))
                .icd("12345")
                .place("Konstanz")
                .build();

        sut.save(diagnosis);

        List<Diagnosis> all = sut.findAll();

        assertTrue(all.contains(diagnosis));
    }
}
