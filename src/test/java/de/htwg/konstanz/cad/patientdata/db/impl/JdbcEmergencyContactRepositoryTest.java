package de.htwg.konstanz.cad.patientdata.db.impl;

import de.htwg.konstanz.cad.patientdata.api.model.EmergencyContact;
import de.htwg.konstanz.cad.patientdata.api.model.ImmutableEmergencyContact;
import de.htwg.konstanz.cad.patientdata.config.PatientDataConfiguration;
import de.htwg.konstanz.cad.provisioning.config.IntegrationTestProvisioningConfiguration;
import de.htwg.konstanz.cad.util.test.IntegrationTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ExtendWith(SpringExtension.class)
@JdbcTest(excludeAutoConfiguration = LiquibaseAutoConfiguration.class)
@ContextConfiguration(classes = {PatientDataConfiguration.class, IntegrationTestProvisioningConfiguration.class})
@AutoConfigureTestDatabase(replace = NONE)
@TestPropertySource(locations = "classpath:test-application.properties")
@IntegrationTest
class JdbcEmergencyContactRepositoryTest {

    @Autowired
    JdbcEmergencyContactRepository sut;

    @Test
    void save() {
        EmergencyContact emergencyContact = ImmutableEmergencyContact.builder()
                .id(UUID.randomUUID())
                .prename("Horst")
                .lastname("Forst")
                .email("horst@forst.de")
                .build();
        sut.save(emergencyContact);
    }

    @Test
    void findAll() {

        EmergencyContact emergencyContact = ImmutableEmergencyContact.builder()
                .id(UUID.randomUUID())
                .prename("Horst")
                .lastname("Forst")
                .email("horst@forst.de")
                .build();
        sut.save(emergencyContact);
        assertTrue(sut.findAll().contains(emergencyContact));
    }
}
