package de.htwg.konstanz.cad.patientdata.db.impl;

import de.htwg.konstanz.cad.patientdata.api.model.ImmutablePatient;
import de.htwg.konstanz.cad.patientdata.api.model.Patient;
import de.htwg.konstanz.cad.patientdata.api.model.Sex;
import de.htwg.konstanz.cad.patientdata.config.PatientDataConfiguration;
import de.htwg.konstanz.cad.provisioning.config.IntegrationTestProvisioningConfiguration;
import de.htwg.konstanz.cad.util.test.IntegrationTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.ZonedDateTime;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;


@ExtendWith(SpringExtension.class)
@JdbcTest(excludeAutoConfiguration = LiquibaseAutoConfiguration.class)
@ContextConfiguration(classes = {PatientDataConfiguration.class, IntegrationTestProvisioningConfiguration.class})
@AutoConfigureTestDatabase(replace = NONE)
@TestPropertySource(locations = "classpath:test-application.properties")
@IntegrationTest
class JdbcPatientRepositoryTest {

    @Autowired
    public JdbcPatientRepository sut;

    @Test
    void save() {

        Patient patient = ImmutablePatient.builder()
                .id(UUID.randomUUID())
                .sex(Sex.M)
                .email("wurst@fing.er")
                .prename("wurst")
                .lastname("finger")
                .placeOfBirth("Schnitzelhausen")
                .dateOfBirth(ZonedDateTime.now())
                .bloodFormula("A0")
                .healthInsurance("Helsana")
                .build();

        sut.save(patient);
    }

    @Test
    void get() {

        Patient patient = ImmutablePatient.builder()
                .id(UUID.randomUUID())
                .sex(Sex.M)
                .email("wurst@fing.er")
                .prename("wurst")
                .lastname("finger")
                .placeOfBirth("Schnitzelhausen")
                .dateOfBirth(ZonedDateTime.now().withNano(0))
                .bloodFormula("A0")
                .healthInsurance("Helsana")
                .build();

        sut.save(patient);
        assertEquals(patient, sut.get());
    }
}
