package de.htwg.konstanz.cad.patientdata.db.impl;

import de.htwg.konstanz.cad.patientdata.api.model.Document;
import de.htwg.konstanz.cad.patientdata.api.model.ImmutableDocument;
import de.htwg.konstanz.cad.patientdata.config.PatientDataConfiguration;
import de.htwg.konstanz.cad.provisioning.config.IntegrationTestProvisioningConfiguration;
import de.htwg.konstanz.cad.tenant.db.config.TenantDbConfiguration;
import de.htwg.konstanz.cad.util.test.IntegrationTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ExtendWith(SpringExtension.class)
@JdbcTest(excludeAutoConfiguration = LiquibaseAutoConfiguration.class)
@ContextConfiguration(classes = {PatientDataConfiguration.class, IntegrationTestProvisioningConfiguration.class})
@AutoConfigureTestDatabase(replace = NONE)
@TestPropertySource(locations = "classpath:test-application.properties")
@IntegrationTest
class JdbcDocumentRepositoryTest {

    @Autowired
    public JdbcDocumentRepository sut;

    @Test
    void save() {
        Document document = ImmutableDocument.builder()
                .id(UUID.randomUUID())
                .title("title")
                .author("author")
                .date(ZonedDateTime.now())
                .description("description")
                .build();

        sut.save(document, Optional.empty());
    }

    @Test
    void findAll() {
        Document document = ImmutableDocument.builder()
                .id(UUID.randomUUID())
                .title("title")
                .author("author")
                .date(ZonedDateTime.now())
                .description("description")
                .build();

        sut.save(document, Optional.empty());
        List<Document> all = sut.findAll();

        assertTrue(all.size() >= 1);
    }
}
