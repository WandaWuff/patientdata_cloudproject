package de.htwg.konstanz.cad.tenant.api.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.htwg.konstanz.cad.util.json.JsonHelper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TenantTest {

    @Test
    void toJson() throws JsonProcessingException {
        Tenant tenant = ImmutableTenant.builder()
                .domain("philip.com")
                .build();

        String string = new JsonHelper().newObjectMapper().writeValueAsString(tenant);
        assertEquals("{\"domain\":\"philip.com\",\"schemaName\":\"philip_com\",\"storageName\":\"philip-com\"}", string);
    }

    @Test
    void fromJson() throws JsonProcessingException {
        Tenant tenant = ImmutableTenant.builder()
                .domain("philip.com")
                .build();

        ObjectMapper objectMapper = new JsonHelper().newObjectMapper();
        String string = objectMapper.writeValueAsString(tenant);
        Tenant tenantFromJson = objectMapper.readValue(string, Tenant.class);
        assertEquals(tenant, tenantFromJson);
    }

    @Test
    void name() {
        Tenant tenant = ImmutableTenant.builder()
                .domain("good@philip.com")
                .build();

        assertEquals("good-philip-com", tenant.getStorageName());
    }
}
