package de.htwg.konstanz.cad.tenant.db.config.service;

import de.htwg.konstanz.cad.provisioning.config.IntegrationTestProvisioningConfiguration;
import de.htwg.konstanz.cad.tenant.api.model.CompanyType;
import de.htwg.konstanz.cad.tenant.api.model.ImmutableTenant;
import de.htwg.konstanz.cad.tenant.api.model.ImmutableTermsAndConditions;
import de.htwg.konstanz.cad.tenant.api.model.Tenant;
import de.htwg.konstanz.cad.tenant.db.config.TenantDbConfiguration;
import de.htwg.konstanz.cad.tenant.db.impl.service.JdbcTenantRepository;
import de.htwg.konstanz.cad.util.test.IntegrationTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.JdbcTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.OffsetDateTime;
import java.util.Arrays;

import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ExtendWith(SpringExtension.class)
@JdbcTest(excludeAutoConfiguration = LiquibaseAutoConfiguration.class)
@ContextConfiguration(classes = {TenantDbConfiguration.class, IntegrationTestProvisioningConfiguration.class})
@AutoConfigureTestDatabase(replace = NONE)
@TestPropertySource(locations = "classpath:test-application.properties")
@IntegrationTest
class JdbcTenantRepositoryTest {

    @Autowired
    public JdbcTenantRepository sut;

    @Test
    void save() {
        Tenant tenant = ImmutableTenant.builder()
                .domain("philip.com")
                .clientSecret("secret")
                .build();
        sut.save(tenant);
    }

    @Test
    void save_no_terms() {
        Tenant tenant = ImmutableTenant.builder()
                .domain("philip.com")
                .clientSecret("secret")
                .build();
        sut.save(tenant);
    }


}
