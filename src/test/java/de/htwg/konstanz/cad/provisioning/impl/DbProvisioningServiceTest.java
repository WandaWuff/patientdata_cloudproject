package de.htwg.konstanz.cad.provisioning.impl;

import de.htwg.konstanz.cad.util.provisioning.DbContext;
import de.htwg.konstanz.cad.util.provisioning.ImmutableDbContext;
import liquibase.exception.LiquibaseException;
import liquibase.integration.spring.SpringLiquibase;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ResourceLoader;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.*;

class DbProvisioningServiceTest {

    DbProvisioningService sut;

    @RepeatedTest(value = 10)
    void test_execution_order() {
        DbContext dbContexts0 = ImmutableDbContext.builder()
                .dataSource(mock(DataSource.class))
                .isTenantAware(false)
                .liquibaseCatalogName("a")
                .build();

        DbContext dbContexts1 = ImmutableDbContext.builder()
                .dataSource(mock(DataSource.class))
                .isTenantAware(false)
                .liquibaseCatalogName("c")
                .build();

        DbContext dbContexts2 = ImmutableDbContext.builder()
                .dataSource(mock(DataSource.class))
                .isTenantAware(true)
                .liquibaseCatalogName("b")
                .build();

        DbContext dbContexts3 = ImmutableDbContext.builder()
                .dataSource(mock(DataSource.class))
                .isTenantAware(true)
                .liquibaseCatalogName("d")
                .build();


        List<DbContext> list = new ArrayList<>(Arrays.asList(dbContexts0, dbContexts1, dbContexts2, dbContexts3));
        Collections.shuffle(list); // randomize

        sut = new DbProvisioningService(null, new LiquibaseHelper(), list, null);

        List<DbContext> dbContexts = sut.orderExecDbContext();

        assertEquals(dbContexts0, dbContexts.get(0));
        assertEquals(dbContexts1, dbContexts.get(1));
        assertEquals(dbContexts2, dbContexts.get(2));
        assertEquals(dbContexts3, dbContexts.get(3));
    }

    @Test
    void createCustomerSchema() throws LiquibaseException, ProvisioningException {

        TenantSchemaSupplier tenantSchemaSupplier = mock(TenantSchemaSupplier.class);
        LiquibaseHelper liquibaseHelper = mock(LiquibaseHelper.class);

        doReturn(mock(SpringLiquibase.class))
                .when(liquibaseHelper)
                .getLiquibaseForDbAndCustomer(
                        notNull(),
                        notNull(),
                        eq("a"),
                        eq("philip_com")
                );

        SchemaCreator schemaCreator = mock(SchemaCreator.class);

        List<DbContext> dbContexts = Collections.singletonList(
                ImmutableDbContext.builder()
                        .dataSource(mock(DataSource.class))
                        .isTenantAware(true)
                        .liquibaseCatalogName("a")
                        .build()
        );

        sut = new DbProvisioningService(tenantSchemaSupplier, liquibaseHelper, dbContexts, schemaCreator);

        sut.setResourceLoader(mock(ResourceLoader.class));

        sut.createTenantSchema("philip_com");

        verify(tenantSchemaSupplier, never()).getTenantSchemaNames();
        verify(schemaCreator).createSchemaIfAbsent(notNull(), eq("philip_com"));
    }
}
