package de.htwg.konstanz.cad.provisioning.security.impl;

import com.google.common.base.Strings;
import org.junit.jupiter.api.Test;

import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

class SecurityTenantServiceImplTest {

    @Test
    void name() {
        Function<String, Integer> a = String::length;
        Function<Integer, String> b = integer -> Strings.repeat("I am a unicorn!", integer);

        Function<String, String> compose = b.compose(a);
    }
}
