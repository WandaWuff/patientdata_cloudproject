package de.htwg.konstanz.cad.provisioning.impl;

import de.htwg.konstanz.cad.provisioning.api.ImmutableUser;
import de.htwg.konstanz.cad.provisioning.api.Role;
import de.htwg.konstanz.cad.provisioning.api.User;
import de.htwg.konstanz.cad.provisioning.storage.api.StorageProvisioningService;
import de.htwg.konstanz.cad.tenant.api.model.ImmutableTenant;
import de.htwg.konstanz.cad.provisioning.security.api.SecurityTenantService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockingDetails;

class ProvisioningServiceImplTest {

    ProvisioningServiceImpl sut;

    @BeforeEach
    void setUp() {
        DbProvisioningService dbProvisioningService = mock(DbProvisioningService.class);
        SecurityTenantService securityTenantService = mock(SecurityTenantService.class);
        StorageProvisioningService storageProvisioningService = mock(StorageProvisioningService.class);
        sut = new ProvisioningServiceImpl(dbProvisioningService, securityTenantService, storageProvisioningService);
    }

    @Test
    void provisionNewCustomer() throws ProvisioningException {
        ImmutableTenant build = ImmutableTenant.builder()
                .domain("philip.com")
                .build();

        User user = ImmutableUser.builder()
                .firstName("first")
                .lastName("last")
                .addRoles(Role.USER)
                .email("first@last.com")
                .password("secret")
                .build();

        sut.provisionNewTenant(build, user);
    }
}
