package de.htwg.konstanz.cad.tenant.api.model;

import java.util.Arrays;
import java.util.NoSuchElementException;

public enum CompanyType {
    COMPANY("company"), START_UP("start-up"), NON_PROFIT("non-profit");

    private final String type;

    CompanyType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public static CompanyType getByType(String type) {
        return Arrays.stream(values())
                .filter(companyType -> companyType.getType().equals(type))
                .findFirst()
                .orElseThrow(NoSuchElementException::new);
    }
}
