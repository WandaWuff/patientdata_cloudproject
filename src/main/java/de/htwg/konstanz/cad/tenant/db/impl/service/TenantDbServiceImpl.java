package de.htwg.konstanz.cad.tenant.db.impl.service;

import de.htwg.konstanz.cad.tenant.api.model.Tenant;
import de.htwg.konstanz.cad.tenant.db.api.service.TenantDbService;

import java.util.List;

public class TenantDbServiceImpl implements TenantDbService {

    private final JdbcTenantRepository jdbcTenantRepository;

    public TenantDbServiceImpl(JdbcTenantRepository jdbcTenantRepository) {
        this.jdbcTenantRepository = jdbcTenantRepository;
    }

    @Override
    public List<String> getAllSchemaNames() {
        return jdbcTenantRepository.getAllSchemaNames();
    }

    @Override
    public void save(Tenant tenant) {
        jdbcTenantRepository.save(tenant);
    }

    @Override
    public boolean exists(String domain) {
        return jdbcTenantRepository.exists(domain);
    }

    @Override
    public Tenant get(String domain) {
        return jdbcTenantRepository.get(domain);
    }
}
