package de.htwg.konstanz.cad.tenant.api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.base.Preconditions;
import org.immutables.value.Value;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

@Value.Immutable
@JsonSerialize(as = ImmutableTenant.class)
@JsonDeserialize(as = ImmutableTenant.class)
public interface Tenant {
    String getDomain();

    @Value.Derived
    default String getSchemaName() {
        String domain = getDomain();
        String objectIdCandidate = domain.replaceAll("[^a-z0-9_]", "_");

        if (Character.isLetter(objectIdCandidate.codePointAt(0))) {
            return objectIdCandidate;
        }

        return "z_" + objectIdCandidate;
    }

    @Value.Derived
    default String getStorageName() {
        String domain = getDomain();
        String storageIdCandidate = domain.replaceAll("[^a-z0-9\\-]", "-");

        if (!Character.isLetter(storageIdCandidate.codePointAt(0))) {
            storageIdCandidate = "z-" + storageIdCandidate;
        }

        if (doesNotEndWithLowerCaseLetterOrDigit(storageIdCandidate)) {
            return storageIdCandidate + "-z";
        }

        return storageIdCandidate;
    }

    private boolean doesNotEndWithLowerCaseLetterOrDigit(String storageIdCandidate) {
        int lastCodePoint = storageIdCandidate.codePoints()
                .reduce((first, second) -> second)
                .orElse("a".codePointAt(0));

        if(Character.isLetter(lastCodePoint) && Character.isLowerCase(lastCodePoint)) {
            return false;
        }

        return ! Character.isDigit(lastCodePoint);
    }

    @JsonIgnore
    Optional<String> getClientSecret();

//    @Value.Check
//    default void checkDomains() {
//        Stream.concat(
//                Stream.of(getDomain()),
//                getOtherDomains().stream()
//        ).forEach(domain -> {
//            Preconditions.checkState(!domain.isBlank(), "domains are not allowed to be blank.");
//
//            Preconditions.checkState(
//                    domain.matches("^(?!://)([a-zA-Z0-9-_]+\\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\\.[a-zA-Z]{2,11}?"),
//                    "The domain " + domain + " seems not to be valid"
//            );
//        });
//    }

    @Value.Check
    default void checkSchemaName() {
        String schemaName = getSchemaName();
        Preconditions.checkState(
                schemaName.matches("[a-z][a-z0-9_]+"),
                "not a valid schema identifier as described in https://www.postgresql.org/docs/12/sql-syntax-lexical.html"
        );
    }
}
