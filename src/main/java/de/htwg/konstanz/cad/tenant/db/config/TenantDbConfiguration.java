package de.htwg.konstanz.cad.tenant.db.config;

import de.htwg.konstanz.cad.tenant.db.api.service.TenantDbService;
import de.htwg.konstanz.cad.tenant.db.impl.service.TenantDbServiceImpl;
import de.htwg.konstanz.cad.tenant.db.impl.service.JdbcTenantRepository;
import de.htwg.konstanz.cad.util.provisioning.DbContext;
import de.htwg.konstanz.cad.util.provisioning.ImmutableDbContext;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

@Configuration
@EnableConfigurationProperties
public class TenantDbConfiguration {

    @Bean
    TenantDbService tenantDbService(JdbcTenantRepository jdbcTenantRepository) {
        return new TenantDbServiceImpl(jdbcTenantRepository);
    }

    @Bean
    JdbcTenantRepository jdbctenantRepository(
            @Qualifier("tenantJdbcTemplate") NamedParameterJdbcTemplate tenantJdbcTemplate
    ) {
        return new JdbcTenantRepository(tenantJdbcTemplate);
    }

    @Bean(name = "tenantJdbcTemplate")
    NamedParameterJdbcTemplate namedParameterJdbcTemplate(@Qualifier("tenantDataSource") DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean(name = "tenantDbContext")
    DbContext dbContext(@Qualifier("tenantDataSource") DataSource dataSource) {
        return ImmutableDbContext.builder()
                .isTenantAware(false)
                .liquibaseCatalogName("tenant")
                .dataSource(dataSource)
                .build();
    }

    @Bean(name = "tenantDataSource")
    DataSource tenantDataSource(
            @Value("${tenant.datasource.driver-class-name}") String driverClassName,
            @Value("${tenant.datasource.username}") String username,
            @Value("${tenant.datasource.password}") String password,
            @Value("${tenant.datasource.jdbc-url}") String jdbcUrl
    ) {
        return DataSourceBuilder.create()
                .username(username)
                .password(password)
                .url(jdbcUrl)
                .driverClassName(driverClassName)
                .build();
    }
}
