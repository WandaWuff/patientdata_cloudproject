package de.htwg.konstanz.cad.tenant.db.impl.service;

import com.google.common.collect.ImmutableList;
import de.htwg.konstanz.cad.tenant.api.model.CompanyType;
import de.htwg.konstanz.cad.tenant.api.model.ImmutableTenant;
import de.htwg.konstanz.cad.tenant.api.model.Tenant;
import de.htwg.konstanz.cad.tenant.api.model.TermsAndConditions;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.Array;
import java.util.*;

public class JdbcTenantRepository {
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public JdbcTenantRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<String> getAllSchemaNames() {
        List<String> strings = jdbcTemplate.queryForList(
                "SELECT schema_name " +
                        "FROM tenant",
                new EmptySqlParameterSource(),
                String.class
        );

        return ImmutableList.copyOf(strings);
    }

    public boolean exists(String domain) {
        Boolean result = jdbcTemplate.queryForObject(
                "SELECT exists(" +
                        "SELECT 1 " +
                        "    FROM tenant " +
                        "    WHERE domain = :domain ) as exists",
                new MapSqlParameterSource("domain", domain),
                Boolean.class);

        return result != null && result;
    }

    public void save(Tenant tenant) {

        Map<String, Object> parameters = Map.of(
                "domain", tenant.getDomain(),
                "schema_name", tenant.getSchemaName(),
                "client_secret", tenant.getClientSecret().orElseThrow(NoSuchElementException::new)
        );

        jdbcTemplate.update(
                "INSERT INTO tenant (domain, schema_name, client_secret) " +
                        "VALUES (:domain, :schema_name, :client_secret) " +
                        "ON CONFLICT (domain) DO UPDATE SET schema_name   = excluded.schema_name, " +
                        "                                   client_secret = excluded.client_secret " ,
                new MapSqlParameterSource(parameters));
    }

    public Tenant get(String domain) {
        return jdbcTemplate.queryForObject(
                "SELECT domain, client_secret FROM tenant WHERE domain=:domain",
                Map.of("domain", domain),
                (rs, rowNum) ->
                        ImmutableTenant.builder()
                                .domain(rs.getString("domain"))
                                .clientSecret(rs.getString("client_secret"))
                                .build()
        );
    }
}
