package de.htwg.konstanz.cad.tenant.api.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.time.OffsetDateTime;

@Value.Immutable
@JsonSerialize(as = ImmutableTermsAndConditions.class)
@JsonDeserialize(as = ImmutableTermsAndConditions.class)
public interface TermsAndConditions {

    String getVersion();

    OffsetDateTime getDateOfAgreement();

    String getPerson();
}
