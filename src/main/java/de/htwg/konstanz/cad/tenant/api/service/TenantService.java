package de.htwg.konstanz.cad.tenant.api.service;

import de.htwg.konstanz.cad.tenant.api.model.Tenant;

import java.util.List;

public interface TenantService {
    List<String> getAllSchemaNames();

    boolean exists(String domain);

    void save(Tenant tenant);

    Tenant get(String domain);
}
