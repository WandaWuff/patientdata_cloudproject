package de.htwg.konstanz.cad.tenant.db.api.service;

import de.htwg.konstanz.cad.tenant.api.model.Tenant;

import java.util.List;

public interface TenantDbService {
    List<String> getAllSchemaNames();

    void save(Tenant tenant);

    boolean exists(String domain);

    Tenant get(String domain);
}
