package de.htwg.konstanz.cad.tenant.impl.service;

import de.htwg.konstanz.cad.tenant.api.model.Tenant;
import de.htwg.konstanz.cad.tenant.api.service.TenantService;
import de.htwg.konstanz.cad.tenant.db.api.service.TenantDbService;

import java.util.List;

public class TenantServiceImpl implements TenantService {

    private final TenantDbService tenantDbService;

    public TenantServiceImpl(TenantDbService tenantDbService) {
        this.tenantDbService = tenantDbService;
    }

    @Override
    public List<String> getAllSchemaNames() {
        return tenantDbService.getAllSchemaNames();
    }

    @Override
    public boolean exists(String domain) {
        return tenantDbService.exists(domain);
    }

    @Override
    public void save(Tenant tenant) {
        tenantDbService.save(tenant);
    }

    @Override
    public Tenant get(String domain) {
        return tenantDbService.get(domain);
    }
}
