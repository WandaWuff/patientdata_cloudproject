package de.htwg.konstanz.cad.tenant.config;

import de.htwg.konstanz.cad.tenant.api.service.TenantService;
import de.htwg.konstanz.cad.tenant.db.config.TenantDbConfiguration;
import de.htwg.konstanz.cad.tenant.db.api.service.TenantDbService;
import de.htwg.konstanz.cad.tenant.impl.service.TenantServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({TenantDbConfiguration.class})
public class TenantConfiguration {

    @Bean
    TenantService customerService(TenantDbService tenantDbService){
        return new TenantServiceImpl(tenantDbService);
    }
}
