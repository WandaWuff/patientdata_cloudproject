package de.htwg.konstanz.cad.util.json.postgresql;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import org.postgresql.util.PGobject;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class JsonPostgreSqlHelper {

    private final ObjectMapper objectMapper;

    public JsonPostgreSqlHelper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public <T> T getValueFromJson(String json, Class<T> inClass) {
        try {
            return objectMapper.readValue(json, inClass);
        } catch (IOException e) {
            throw new SqlJsonNotReadableException("", e);
        }
    }

    public <T> Iterable<? extends T> getCollectionFromJson(String json, Class<T> inClass) {
        try {
            CollectionType collectionType = objectMapper.getTypeFactory().constructCollectionType(List.class, inClass);
            return objectMapper.readValue(json, collectionType);
        } catch (IOException e) {
            throw new SqlJsonNotReadableException("", e);
        }
    }

    public PGobject getJsonFromObject(Object o) {
        try {
            String json = objectMapper.writeValueAsString(o);

            PGobject jsonbObj = new PGobject();
            jsonbObj.setType("json");
            jsonbObj.setValue(json);
            return jsonbObj;
        } catch (JsonProcessingException | SQLException e) {
            throw new SqlJsonNotWriteableException("could not write json", e);
        }
    }
}
