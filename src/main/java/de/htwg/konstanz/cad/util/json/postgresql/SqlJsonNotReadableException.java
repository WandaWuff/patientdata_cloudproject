package de.htwg.konstanz.cad.util.json.postgresql;

import org.springframework.dao.DataAccessException;

public class SqlJsonNotReadableException extends DataAccessException {

    public SqlJsonNotReadableException(String msg) {
        super(msg);
    }

    public SqlJsonNotReadableException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
