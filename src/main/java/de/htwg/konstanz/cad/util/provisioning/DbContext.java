package de.htwg.konstanz.cad.util.provisioning;

import org.immutables.value.Value;

import javax.sql.DataSource;

@Value.Immutable
public interface DbContext {
    DataSource getDataSource();

    String getLiquibaseCatalogName();

    boolean isTenantAware();
}
