package de.htwg.konstanz.cad.util.tenant;

import java.util.Optional;

public class TenantStore {

    private String tenantId;
    private String schemaName;

    public void clear() {
        this.tenantId = null;
    }

    public Optional<String> getTenantId() {
        return Optional.ofNullable(tenantId);
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public Optional<String> getSchemaName() {
        return Optional.ofNullable(schemaName);
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }
}
