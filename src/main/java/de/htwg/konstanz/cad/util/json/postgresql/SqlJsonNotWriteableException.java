package de.htwg.konstanz.cad.util.json.postgresql;

import org.springframework.dao.DataAccessException;

public class SqlJsonNotWriteableException extends DataAccessException {

    public SqlJsonNotWriteableException(String msg) {
        super(msg);
    }

    public SqlJsonNotWriteableException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
