package de.htwg.konstanz.cad.application;

import de.htwg.konstanz.cad.rs.config.RsConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.context.annotation.Import;

@SpringBootApplication(
        exclude = LiquibaseAutoConfiguration.class
)
@Import({RsConfiguration.class})
public class PatientDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(PatientDataApplication.class, args);
    }
}
