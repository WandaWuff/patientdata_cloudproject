package de.htwg.konstanz.cad.patientdata.db.api;

import de.htwg.konstanz.cad.patientdata.api.model.Document;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface DocumentService {
    void save(Document document, Optional<UUID> diagnosis);

    List<Document> findAll();

    List<Document> findAllDocumentsForDiagnosis(UUID diagnosisId);
}
