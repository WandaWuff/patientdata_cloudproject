package de.htwg.konstanz.cad.patientdata.api.model;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.time.ZonedDateTime;
import java.util.UUID;

@Value.Immutable
@JsonSerialize(as = ImmutableDocument.class)
@JsonDeserialize(as = ImmutableDocument.class)
public interface Document {
    UUID getId();

    String getTitle();

    String getAuthor();

    String getDescription();

    ZonedDateTime getDate();
}
