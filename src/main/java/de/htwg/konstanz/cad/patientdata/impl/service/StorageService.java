package de.htwg.konstanz.cad.patientdata.impl.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public interface StorageService {
    void save(UUID documentId, InputStream file);

    byte[] findFile(UUID documentId) throws IOException;
}
