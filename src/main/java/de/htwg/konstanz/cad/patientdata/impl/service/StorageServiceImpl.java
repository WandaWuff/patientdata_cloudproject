package de.htwg.konstanz.cad.patientdata.impl.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import de.htwg.konstanz.cad.tenant.api.model.Tenant;
import de.htwg.konstanz.cad.tenant.api.service.TenantService;
import de.htwg.konstanz.cad.util.tenant.TenantStore;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

public class StorageServiceImpl implements StorageService {

    private final AmazonS3 amazonS3;
    private final TenantStore tenantStore;
    private final TenantService tenantService;

    public StorageServiceImpl(AmazonS3 amazonS3, TenantStore tenantStore, TenantService tenantService) {
        this.amazonS3 = amazonS3;
        this.tenantStore = tenantStore;
        this.tenantService = tenantService;
    }

    @Override
    public void save(UUID documentId, InputStream file) {
        String storageName = getStorageName();
        amazonS3.putObject(storageName, documentId.toString(), file, new ObjectMetadata());
    }

    @Override
    public byte[] findFile(UUID documentId) throws IOException {
        String storageName = getStorageName();
        S3Object object = amazonS3.getObject(storageName, documentId.toString());
        return object.getObjectContent().readAllBytes();
    }

    private String getStorageName() {
        return tenantStore.getTenantId()
                .map(tenantService::get)
                .map(Tenant::getStorageName)
                .orElseThrow(IllegalStateException::new);
    }
}
