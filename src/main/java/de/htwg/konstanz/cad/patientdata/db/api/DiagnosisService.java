package de.htwg.konstanz.cad.patientdata.db.api;

import de.htwg.konstanz.cad.patientdata.api.model.Diagnosis;

import java.util.List;

public interface DiagnosisService {
    void save(Diagnosis diagnosis);

    List<Diagnosis> findAll();
}
