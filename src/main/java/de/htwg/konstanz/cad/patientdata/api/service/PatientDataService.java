package de.htwg.konstanz.cad.patientdata.api.service;

import de.htwg.konstanz.cad.patientdata.api.model.Diagnosis;
import de.htwg.konstanz.cad.patientdata.api.model.Document;
import de.htwg.konstanz.cad.patientdata.api.model.EmergencyContact;
import de.htwg.konstanz.cad.patientdata.api.model.Patient;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface PatientDataService {
    void save(Diagnosis diagnosis);

    List<Diagnosis> findAllDiagnosis();

    void save(Document document, Optional<UUID> diagnosis);

    List<Document> findAllDocuments();

    void save(UUID documentId, InputStream file);

    byte[] getFile(UUID documentId) throws IOException;

    void save(Patient patient);

    Patient getPatientData();

    void save(EmergencyContact emergencyContact);

    List<EmergencyContact> findAllEmergencyContacts();

    List<Document> findAllDocumentsForDiagnosis(UUID diagnosis);
}
