package de.htwg.konstanz.cad.patientdata.db.api;

import de.htwg.konstanz.cad.patientdata.api.model.EmergencyContact;

import java.util.List;

public interface EmergencyContactService {
    void save(EmergencyContact emergencyContact);

    List<EmergencyContact> findAll();
}
