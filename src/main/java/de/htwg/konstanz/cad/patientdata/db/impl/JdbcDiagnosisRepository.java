package de.htwg.konstanz.cad.patientdata.db.impl;

import de.htwg.konstanz.cad.patientdata.api.model.Diagnosis;
import de.htwg.konstanz.cad.patientdata.api.model.ImmutableDiagnosis;
import de.htwg.konstanz.cad.util.tenant.TenantStore;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class JdbcDiagnosisRepository {
    private final TenantStore tenantStore;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public JdbcDiagnosisRepository(TenantStore tenantStore, NamedParameterJdbcTemplate jdbcTemplate) {
        this.tenantStore = tenantStore;
        this.jdbcTemplate = jdbcTemplate;
    }

    public void save(Diagnosis diagnosis) {
        SqlParameterSource parameterSource = toParameterSource(diagnosis);
        jdbcTemplate.update(
                "INSERT INTO " + getTableName() + " (id, icd, date, place, doctor) " +
                        "VALUES (:id, :icd, :date, :place, :doctor) " +
                        "ON CONFLICT (id) DO UPDATE SET id               = excluded.id, " +
                        "                               icd              = excluded.icd, " +
                        "                               date             = excluded.date, " +
                        "                               place            = excluded.place, " +
                        "                               doctor           = excluded.doctor ",
                parameterSource
        );
    }

    public List<Diagnosis> findAll() {

        return jdbcTemplate.query(
                "SELECT id, icd, date, place, doctor FROM " + getTableName(),
                Map.of(),
                this::mapRow
        );
    }

    private SqlParameterSource toParameterSource(Diagnosis diagnosis) {
        return new MapSqlParameterSource()
                .addValue("id", diagnosis.getId())
                .addValue("icd", diagnosis.getIcd())
                .addValue("date", diagnosis.getDate().toLocalDateTime())
                .addValue("place", diagnosis.getPlace())
                .addValue("doctor", diagnosis.getDoctor())
                ;
    }

    private Diagnosis mapRow(ResultSet rs, int rowNum) throws SQLException {
        return ImmutableDiagnosis.builder()
                .id(UUID.fromString(rs.getString("id")))
                .icd(rs.getString("icd"))
                .date(rs.getTimestamp("date").toLocalDateTime().atZone(ZoneId.systemDefault()))
                .place(rs.getString("place"))
                .doctor(rs.getString("doctor"))
                .build();
    }

    private String getTableName() {
        return tenantStore.getSchemaName().orElseThrow() + ".diagnosis";
    }
}
