package de.htwg.konstanz.cad.patientdata.db.impl;

import de.htwg.konstanz.cad.patientdata.api.model.Document;
import de.htwg.konstanz.cad.patientdata.db.api.DocumentService;

import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class DocumentServiceImpl implements DocumentService {

    private final JdbcDocumentRepository jdbcDocumentRepository;

    public DocumentServiceImpl(JdbcDocumentRepository jdbcDocumentRepository) {
        this.jdbcDocumentRepository = jdbcDocumentRepository;
    }

    @Override
    public void save(Document document, Optional<UUID> diagnosis) {
        jdbcDocumentRepository.save(document, diagnosis);
    }

    @Override
    public List<Document> findAll() {
        return jdbcDocumentRepository.findAll();
    }

    @Override
    public List<Document> findAllDocumentsForDiagnosis(UUID diagnosisId) {
        return jdbcDocumentRepository.findAllDocumentsForDiagnosis(diagnosisId);
    }
}
