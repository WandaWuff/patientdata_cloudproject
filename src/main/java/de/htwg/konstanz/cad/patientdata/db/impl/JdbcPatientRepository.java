package de.htwg.konstanz.cad.patientdata.db.impl;

import de.htwg.konstanz.cad.patientdata.api.model.ImmutablePatient;
import de.htwg.konstanz.cad.patientdata.api.model.Patient;
import de.htwg.konstanz.cad.patientdata.api.model.Sex;
import de.htwg.konstanz.cad.util.tenant.TenantStore;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZoneId;
import java.util.Map;
import java.util.UUID;

public class JdbcPatientRepository {

    private final TenantStore tenantStore;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public JdbcPatientRepository(TenantStore tenantStore, NamedParameterJdbcTemplate jdbcTemplate) {
        this.tenantStore = tenantStore;
        this.jdbcTemplate = jdbcTemplate;
    }

    public void save(Patient patient) {
        SqlParameterSource parameterSource = toParameterSource(patient);
        jdbcTemplate.update(
                "INSERT INTO " + getTableName() + " (id, prename, lastname, sex, date_of_birth, place_of_birth, health_insurance, blood_formular, email) " +
                        "VALUES (:id, :prename, :lastname, :sex, :date_of_birth, :place_of_birth, :health_insurance, :blood_formular, :email) " +
                        "ON CONFLICT (id) DO UPDATE SET id               = excluded.id, " +
                        "                               prename          = excluded.prename, " +
                        "                               lastname         = excluded.lastname, " +
                        "                               sex              = excluded.sex, " +
                        "                               date_of_birth    = excluded.date_of_birth, " +
                        "                               place_of_birth   = excluded.place_of_birth, " +
                        "                               health_insurance = excluded.health_insurance, " +
                        "                               blood_formular   = excluded.blood_formular, " +
                        "                               email            = excluded.email",
                parameterSource
        );
    }

    public Patient get() {
        return jdbcTemplate.queryForObject(
                "SELECT id, prename, lastname, sex, date_of_birth, place_of_birth, health_insurance, blood_formular, email FROM " + getTableName(),
                Map.of(),
                this::mapRow
        );
    }

    private SqlParameterSource toParameterSource(Patient patient) {
        return new MapSqlParameterSource()
                .addValue("id", patient.getId())
                .addValue("prename", patient.getPrename())
                .addValue("lastname", patient.getLastname())
                .addValue("sex", patient.getSex().name())
                .addValue("date_of_birth", patient.getDateOfBirth().toLocalDateTime())
                .addValue("place_of_birth", patient.getPlaceOfBirth())
                .addValue("health_insurance", patient.getHealthInsurance())
                .addValue("blood_formular", patient.getBloodFormula())
                .addValue("email", patient.getEmail())
                ;
    }

    private Patient mapRow(ResultSet rs, int rowNum) throws SQLException {
        return ImmutablePatient.builder()
                .id(UUID.fromString(rs.getString("id")))
                .prename(rs.getString("prename"))
                .lastname(rs.getString("lastname"))
                .sex(Sex.valueOf(rs.getString("sex")))
                .dateOfBirth(rs.getTimestamp("date_of_birth").toLocalDateTime().atZone(ZoneId.systemDefault()))
                .placeOfBirth(rs.getString("place_of_birth"))
                .healthInsurance(rs.getString("health_insurance"))
                .bloodFormula(rs.getString("blood_formular"))
                .email(rs.getString("email"))
                .build();
    }

    private String getTableName() {
        return tenantStore.getSchemaName().orElseThrow() + ".patient";
    }
}
