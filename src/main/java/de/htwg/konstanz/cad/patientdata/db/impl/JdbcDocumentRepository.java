package de.htwg.konstanz.cad.patientdata.db.impl;

import de.htwg.konstanz.cad.patientdata.api.model.Document;
import de.htwg.konstanz.cad.patientdata.api.model.ImmutableDocument;
import de.htwg.konstanz.cad.util.tenant.TenantStore;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.ZoneId;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class JdbcDocumentRepository {

    private final TenantStore tenantStore;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public JdbcDocumentRepository(TenantStore tenantStore, NamedParameterJdbcTemplate jdbcTemplate) {
        this.tenantStore = tenantStore;
        this.jdbcTemplate = jdbcTemplate;
    }

    public void save(Document document, Optional<UUID> diagnosis) {
        SqlParameterSource parameterSource = toParameterSource(document, diagnosis);
        jdbcTemplate.update(
                "INSERT INTO " + getTableName() + " (id, title, author, description, date, diagnosis_id) " +
                        "VALUES (:id, :title, :author, :description, :date, :diagnosis_id) " +
                        "ON CONFLICT (id) DO UPDATE SET id                 = excluded.id, " +
                        "                               title              = excluded.title, " +
                        "                               author             = excluded.author, " +
                        "                               description        = excluded.description, " +
                        "                               date               = excluded.date, " +
                        "                               diagnosis_id       = excluded.diagnosis_id",
                parameterSource
        );
    }

    public List<Document> findAll() {
        return jdbcTemplate.query(
                "SELECT id, title, author, description, date, diagnosis_id FROM " + getTableName(),
                Map.of(),
                this::mapRow
        );
    }

    public List<Document> findAllDocumentsForDiagnosis(UUID diagnosisId) {
        return jdbcTemplate.query(
                "SELECT id, title, author, description, date, diagnosis_id FROM " + getTableName() + " WHERE diagnosis_id = :diagnosis_id",
                Map.of("diagnosis_id", diagnosisId),
                this::mapRow
        );
    }

    private SqlParameterSource toParameterSource(Document document, Optional<UUID> diagnosis) {
        return new MapSqlParameterSource()
                .addValue("id", document.getId())
                .addValue("title", document.getTitle())
                .addValue("author", document.getAuthor())
                .addValue("description", document.getDescription())
                .addValue("date", document.getDate().toLocalDateTime())
                .addValue("diagnosis_id", diagnosis.orElse(null))
                ;
    }

    private Document mapRow(ResultSet rs, int rowNum) throws SQLException {
        return ImmutableDocument.builder()
                .id(UUID.fromString(rs.getString("id")))
                .title(rs.getString("title"))
                .author(rs.getString("author"))
                .description(rs.getString("description"))
                .date(rs.getTimestamp("date").toLocalDateTime().atZone(ZoneId.systemDefault()))
                .build();
    }

    private String getTableName() {
        return tenantStore.getSchemaName().orElseThrow() + ".document";
    }
}
