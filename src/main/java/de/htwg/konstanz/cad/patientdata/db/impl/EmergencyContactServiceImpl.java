package de.htwg.konstanz.cad.patientdata.db.impl;

import de.htwg.konstanz.cad.patientdata.api.model.EmergencyContact;
import de.htwg.konstanz.cad.patientdata.db.api.EmergencyContactService;

import java.util.List;

public class EmergencyContactServiceImpl implements EmergencyContactService {

    private final JdbcEmergencyContactRepository jdbcEmergencyContactRepository;

    public EmergencyContactServiceImpl(JdbcEmergencyContactRepository jdbcEmergencyContactRepository) {
        this.jdbcEmergencyContactRepository = jdbcEmergencyContactRepository;
    }

    @Override
    public void save(EmergencyContact emergencyContact) {
        jdbcEmergencyContactRepository.save(emergencyContact);
    }

    @Override
    public List<EmergencyContact> findAll() {
        return jdbcEmergencyContactRepository.findAll();
    }
}
