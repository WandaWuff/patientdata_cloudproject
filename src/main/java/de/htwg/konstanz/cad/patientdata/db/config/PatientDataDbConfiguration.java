package de.htwg.konstanz.cad.patientdata.db.config;

import de.htwg.konstanz.cad.patientdata.db.api.DiagnosisService;
import de.htwg.konstanz.cad.patientdata.db.api.DocumentService;
import de.htwg.konstanz.cad.patientdata.db.api.EmergencyContactService;
import de.htwg.konstanz.cad.patientdata.db.api.PatientService;
import de.htwg.konstanz.cad.patientdata.db.impl.*;
import de.htwg.konstanz.cad.util.provisioning.DbContext;
import de.htwg.konstanz.cad.util.provisioning.ImmutableDbContext;
import de.htwg.konstanz.cad.util.tenant.TenantStore;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class PatientDataDbConfiguration {
    @Bean
    DiagnosisService diagnosisService(JdbcDiagnosisRepository jdbcDiagnosisRepository) {
        return new DiagnosisServiceImpl(jdbcDiagnosisRepository);
    }

    @Bean
    DocumentService documentService(JdbcDocumentRepository jdbcDocumentRepository) {
        return new DocumentServiceImpl(jdbcDocumentRepository);
    }

    @Bean
    EmergencyContactService emergencyContactService(JdbcEmergencyContactRepository jdbcEmergencyContactRepository) {
        return new EmergencyContactServiceImpl(jdbcEmergencyContactRepository);
    }

    @Bean
    PatientService patientService(JdbcPatientRepository jdbcPatientRepository) {
        return new PatientServiceImpl(jdbcPatientRepository);
    }

    @Bean
    JdbcEmergencyContactRepository jdbcEmergencyContactRepository(
            @Qualifier("patientdataJdbcTemplate") NamedParameterJdbcTemplate jdbcTemplate,
            TenantStore tenantStore
    ) {
        return new JdbcEmergencyContactRepository(tenantStore, jdbcTemplate);
    }

    @Bean
    JdbcPatientRepository jdbcPatientRepository(
            @Qualifier("patientdataJdbcTemplate") NamedParameterJdbcTemplate jdbcTemplate,
            TenantStore tenantStore
    ) {
        return new JdbcPatientRepository(tenantStore, jdbcTemplate);
    }

    @Bean
    JdbcDiagnosisRepository jdbcDiagnosisRepository(
            @Qualifier("patientdataJdbcTemplate") NamedParameterJdbcTemplate jdbcTemplate,
            TenantStore tenantStore
    ) {
        return new JdbcDiagnosisRepository(tenantStore, jdbcTemplate);
    }

    @Bean
    JdbcDocumentRepository jdbcDocumentRepository(
            @Qualifier("patientdataJdbcTemplate") NamedParameterJdbcTemplate jdbcTemplate,
            TenantStore tenantStore
    ) {
        return new JdbcDocumentRepository(tenantStore, jdbcTemplate);
    }


    @Bean(name = "patientdataJdbcTemplate")
    NamedParameterJdbcTemplate jdbcTemplate(
            @Qualifier("patientdataDataSource") DataSource dataSource
    ) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean(name = "patientdataDbContext")
    DbContext dbContext(@Qualifier("patientdataDataSource") DataSource dataSource) {
        return ImmutableDbContext.builder()
                .dataSource(dataSource)
                .isTenantAware(true)
                .liquibaseCatalogName("patientdata")
                .build();
    }

    @Bean(name = "patientdataDataSource")
    DataSource dataSource(
            @Value("${patientdata.datasource.driver-class-name}") String driverClassName,
            @Value("${patientdata.datasource.username}") String username,
            @Value("${patientdata.datasource.password}") String password,
            @Value("${patientdata.datasource.jdbc-url}") String jdbcUrl
    ) {
        return DataSourceBuilder.create()
                .username(username)
                .password(password)
                .url(jdbcUrl)
                .driverClassName(driverClassName)
                .build();
    }
}
