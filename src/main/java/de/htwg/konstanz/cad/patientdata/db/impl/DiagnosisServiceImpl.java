package de.htwg.konstanz.cad.patientdata.db.impl;

import de.htwg.konstanz.cad.patientdata.api.model.Diagnosis;
import de.htwg.konstanz.cad.patientdata.db.api.DiagnosisService;

import java.util.List;

public class DiagnosisServiceImpl implements DiagnosisService {

    private final JdbcDiagnosisRepository jdbcDiagnosisRepository;

    public DiagnosisServiceImpl(JdbcDiagnosisRepository jdbcDiagnosisRepository) {
        this.jdbcDiagnosisRepository = jdbcDiagnosisRepository;
    }

    @Override
    public void save(Diagnosis diagnosis) {
        jdbcDiagnosisRepository.save(diagnosis);
    }

    @Override
    public List<Diagnosis> findAll() {
        return jdbcDiagnosisRepository.findAll();
    }
}
