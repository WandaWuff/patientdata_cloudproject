package de.htwg.konstanz.cad.patientdata.db.impl;

import de.htwg.konstanz.cad.patientdata.api.model.*;
import de.htwg.konstanz.cad.util.tenant.TenantStore;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class JdbcEmergencyContactRepository {

    private final TenantStore tenantStore;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public JdbcEmergencyContactRepository(TenantStore tenantStore, NamedParameterJdbcTemplate jdbcTemplate) {
        this.tenantStore = tenantStore;
        this.jdbcTemplate = jdbcTemplate;
    }

    public void save(EmergencyContact emergencyContact) {
        SqlParameterSource parameterSource = toParameterSource(emergencyContact);
        jdbcTemplate.update(
                "INSERT INTO " + getTableName() + " (id, prename, lastname, email) " +
                        "VALUES (:id, :prename, :lastname, :email) " +
                        "ON CONFLICT (id) DO UPDATE SET id               = excluded.id, " +
                        "                               prename          = excluded.prename, " +
                        "                               lastname         = excluded.lastname, " +
                        "                               email            = excluded.email",
                parameterSource
        );
    }

    private SqlParameterSource toParameterSource(EmergencyContact emergencyContact) {
        return new MapSqlParameterSource()
                .addValue("id", emergencyContact.getId())
                .addValue("prename", emergencyContact.getPrename())
                .addValue("lastname", emergencyContact.getLastname())
                .addValue("email", emergencyContact.getEmail())
                ;
    }

    public List<EmergencyContact> findAll() {
        return jdbcTemplate.query(
                "SELECT id, prename, lastname, email FROM " + getTableName(),
                Map.of(),
                this::mapRow
        );
    }

    private EmergencyContact mapRow(ResultSet rs, int rowNum) throws SQLException {
        return ImmutableEmergencyContact.builder()
                .id(UUID.fromString(rs.getString("id")))
                .prename(rs.getString("prename"))
                .lastname(rs.getString("lastname"))
                .email(rs.getString("email"))
                .build();
    }

    private String getTableName() {
        return tenantStore.getSchemaName().orElseThrow() + ".emergency_contact";
    }
}

