package de.htwg.konstanz.cad.patientdata.db.api;

import de.htwg.konstanz.cad.patientdata.api.model.Patient;

public interface PatientService {
    void save(Patient patient);

    Patient get();
}
