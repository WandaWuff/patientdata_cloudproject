package de.htwg.konstanz.cad.patientdata.db.impl;

import de.htwg.konstanz.cad.patientdata.api.model.Patient;
import de.htwg.konstanz.cad.patientdata.db.api.PatientService;

public class PatientServiceImpl implements PatientService {

    private final JdbcPatientRepository jdbcPatientRepository;

    public PatientServiceImpl(JdbcPatientRepository jdbcPatientRepository) {
        this.jdbcPatientRepository = jdbcPatientRepository;
    }

    @Override
    public void save(Patient patient) {
        jdbcPatientRepository.save(patient);
    }

    @Override
    public Patient get() {
        return jdbcPatientRepository.get();
    }
}
