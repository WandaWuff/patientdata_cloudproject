package de.htwg.konstanz.cad.patientdata.api.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import de.htwg.konstanz.cad.tenant.api.model.ImmutableTenant;
import org.immutables.value.Value;

import java.time.ZonedDateTime;
import java.util.UUID;

@Value.Immutable
@JsonSerialize(as = ImmutableDiagnosis.class)
@JsonDeserialize(as = ImmutableDiagnosis.class)
public interface Diagnosis {
    UUID getId();

    String getIcd();

    ZonedDateTime getDate();

    String getPlace();

    String getDoctor();
}
