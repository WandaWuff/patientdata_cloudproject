package de.htwg.konstanz.cad.patientdata.config;

import com.amazonaws.services.s3.AmazonS3;
import de.htwg.konstanz.cad.patientdata.api.service.PatientDataService;
import de.htwg.konstanz.cad.patientdata.db.api.DiagnosisService;
import de.htwg.konstanz.cad.patientdata.db.api.DocumentService;
import de.htwg.konstanz.cad.patientdata.db.api.EmergencyContactService;
import de.htwg.konstanz.cad.patientdata.db.api.PatientService;
import de.htwg.konstanz.cad.patientdata.db.config.PatientDataDbConfiguration;
import de.htwg.konstanz.cad.patientdata.impl.service.PatientDataServiceImpl;
import de.htwg.konstanz.cad.patientdata.impl.service.StorageService;
import de.htwg.konstanz.cad.patientdata.impl.service.StorageServiceImpl;
import de.htwg.konstanz.cad.tenant.api.service.TenantService;
import de.htwg.konstanz.cad.util.tenant.TenantStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({PatientDataDbConfiguration.class})
public class PatientDataConfiguration {

    @Bean
    PatientDataService patientDataService(
            DiagnosisService diagnosisService,
            DocumentService documentService,
            PatientService patientService,
            EmergencyContactService emergencyContactService,
            StorageService storageService
    ) {

        return new PatientDataServiceImpl(
                diagnosisService,
                documentService,
                patientService,
                emergencyContactService,
                storageService);
    }

    @Bean
    StorageService storageService(AmazonS3 amazonS3, TenantService tenantService, TenantStore tenantStore) {
        return new StorageServiceImpl(amazonS3, tenantStore, tenantService);
    }
}
