package de.htwg.konstanz.cad.patientdata.api.model;

import java.util.UUID;

public interface Contact {
    UUID getId();

    String getPrename();

    String getLastname();

    String getEmail();
}
