package de.htwg.konstanz.cad.patientdata.api.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value.Immutable;

import java.time.ZonedDateTime;

@Immutable
@JsonSerialize(as = ImmutablePatient.class)
@JsonDeserialize(as = ImmutablePatient.class)
public interface Patient extends Contact {

    Sex getSex();

    ZonedDateTime getDateOfBirth();

    String getPlaceOfBirth();

    String getHealthInsurance();

    String getBloodFormula();

}
