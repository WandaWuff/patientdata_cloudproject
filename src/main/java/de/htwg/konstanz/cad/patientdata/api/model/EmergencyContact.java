package de.htwg.konstanz.cad.patientdata.api.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize(as = ImmutableEmergencyContact.class)
@JsonDeserialize(as = ImmutableEmergencyContact.class)
public interface EmergencyContact extends Contact {
}
