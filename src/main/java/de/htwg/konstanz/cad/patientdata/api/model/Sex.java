package de.htwg.konstanz.cad.patientdata.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.STRING)
public enum Sex {
    F, M, X
}
