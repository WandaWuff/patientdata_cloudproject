package de.htwg.konstanz.cad.patientdata.impl.service;

import de.htwg.konstanz.cad.patientdata.api.model.Diagnosis;
import de.htwg.konstanz.cad.patientdata.api.model.Document;
import de.htwg.konstanz.cad.patientdata.api.model.EmergencyContact;
import de.htwg.konstanz.cad.patientdata.api.model.Patient;
import de.htwg.konstanz.cad.patientdata.api.service.PatientDataService;
import de.htwg.konstanz.cad.patientdata.db.api.DiagnosisService;
import de.htwg.konstanz.cad.patientdata.db.api.DocumentService;
import de.htwg.konstanz.cad.patientdata.db.api.EmergencyContactService;
import de.htwg.konstanz.cad.patientdata.db.api.PatientService;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class PatientDataServiceImpl implements PatientDataService {

    private final DiagnosisService diagnosisService;
    private final DocumentService documentService;
    private final PatientService patientService;
    private final EmergencyContactService emergencyContactService;
    private final StorageService storageService;

    public PatientDataServiceImpl(
            DiagnosisService diagnosisService,
            DocumentService documentService,
            PatientService patientService,
            EmergencyContactService emergencyContactService, StorageService storageService) {

        this.diagnosisService = diagnosisService;
        this.documentService = documentService;
        this.patientService = patientService;
        this.emergencyContactService = emergencyContactService;
        this.storageService = storageService;
    }

    @Override
    public void save(Diagnosis diagnosis) {
        diagnosisService.save(diagnosis);
    }

    @Override
    public List<Diagnosis> findAllDiagnosis() {
        return diagnosisService.findAll();
    }

    @Override
    public void save(Document document, Optional<UUID> diagnosis) {
        documentService.save(document, diagnosis);
    }

    @Override
    public List<Document> findAllDocuments() {
        return documentService.findAll();
    }

    @Override
    public void save(UUID documentId, InputStream file) {
        storageService.save(documentId, file);
    }

    @Override
    public byte[] getFile(UUID documentId) throws IOException {
        return storageService.findFile(documentId);
    }

    @Override
    public void save(Patient patient) {
        patientService.save(patient);
    }

    @Override
    public Patient getPatientData() {
        return patientService.get();
    }

    @Override
    public void save(EmergencyContact emergencyContact) {
        emergencyContactService.save(emergencyContact);
    }

    @Override
    public List<EmergencyContact> findAllEmergencyContacts() {
        return emergencyContactService.findAll();
    }

    @Override
    public List<Document> findAllDocumentsForDiagnosis(UUID diagnosisId) {
        return documentService.findAllDocumentsForDiagnosis(diagnosisId);
    }
}
