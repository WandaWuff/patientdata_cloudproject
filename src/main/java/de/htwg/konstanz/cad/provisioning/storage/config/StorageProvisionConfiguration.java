package de.htwg.konstanz.cad.provisioning.storage.config;

import com.amazonaws.services.s3.AmazonS3;
import de.htwg.konstanz.cad.provisioning.storage.api.StorageProvisioningService;
import de.htwg.konstanz.cad.provisioning.storage.impl.S3ProvisioningServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StorageProvisionConfiguration {

    @Bean
    StorageProvisioningService storageProvisioningService(AmazonS3 amazonS3){
        return new S3ProvisioningServiceImpl(amazonS3);
    }
}
