package de.htwg.konstanz.cad.provisioning.security.config;

import de.htwg.konstanz.cad.provisioning.security.api.ClientSecretRetriever;
import de.htwg.konstanz.cad.provisioning.security.api.SecurityTenantService;
import de.htwg.konstanz.cad.provisioning.security.impl.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(ProvisioningSecurityProperties.class)
public class ProvisioningSecurityConfiguration {

    @Bean
    SecurityTenantService securityTenantService(ProvisioningSecurityProperties securityProperties) {
        return new SecurityTenantServiceImpl(
                securityProperties.getUrl(),
                securityProperties.getMasterRealm(),
                securityProperties.getClientId(),
                securityProperties.getClientSecret(),
                securityProperties.getUsername(),
                securityProperties.getPassword(),
                new RoleBuilder(),
                new ClientBuilder(),
                new UserBuilder());
    }

    @Bean
    public org.keycloak.adapters.KeycloakConfigResolver keycloakConfigResolver(ClientSecretRetriever clientSecretRetriever, @Value("${keycloak.auth-server-url:http://localhost:8888/auth}") String authServerUrl) {
        return new KeycloakConfigResolver(clientSecretRetriever, authServerUrl, "patientdata_backend");
    }


}
