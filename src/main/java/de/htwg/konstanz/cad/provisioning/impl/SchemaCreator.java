package de.htwg.konstanz.cad.provisioning.impl;

import de.htwg.konstanz.cad.util.provisioning.DbContext;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Map;

public class SchemaCreator {

    void createSchemaIfAbsent(DbContext dbContext, String customerSchemaName) {
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dbContext.getDataSource());
        namedParameterJdbcTemplate.update("CREATE SCHEMA IF NOT EXISTS " + customerSchemaName, Map.of());
    }
}
