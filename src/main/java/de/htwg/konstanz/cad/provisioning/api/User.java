package de.htwg.konstanz.cad.provisioning.api;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import java.util.Set;

@Value.Immutable
@JsonSerialize(as = ImmutableUser.class)
@JsonDeserialize(as = ImmutableUser.class)
public interface User {

    String getFirstName();

    String getLastName();

    String getEmail();

    String getPassword();

    Set<Role> getRoles();
}
