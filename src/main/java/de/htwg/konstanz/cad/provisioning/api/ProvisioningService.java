package de.htwg.konstanz.cad.provisioning.api;

import de.htwg.konstanz.cad.provisioning.impl.ProvisioningException;
import de.htwg.konstanz.cad.tenant.api.model.Tenant;

import java.util.List;

public interface ProvisioningService {
    Tenant provisionNewTenant(Tenant tenant, User users) throws ProvisioningException;

    void createUser(Tenant tenant, User user);
}
