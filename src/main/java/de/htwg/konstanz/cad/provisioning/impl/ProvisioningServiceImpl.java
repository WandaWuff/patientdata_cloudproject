package de.htwg.konstanz.cad.provisioning.impl;

import de.htwg.konstanz.cad.provisioning.api.ProvisioningService;
import de.htwg.konstanz.cad.provisioning.api.User;
import de.htwg.konstanz.cad.provisioning.storage.api.StorageProvisioningService;
import de.htwg.konstanz.cad.rs.tenant.impl.TenantControllerImpl;
import de.htwg.konstanz.cad.tenant.api.model.ImmutableTenant;
import de.htwg.konstanz.cad.tenant.api.model.Tenant;
import de.htwg.konstanz.cad.provisioning.security.api.SecurityTenantService;
import liquibase.exception.LiquibaseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.UUID;

public class ProvisioningServiceImpl implements ProvisioningService {

    private final Logger LOGGER = LogManager.getLogger(ProvisioningServiceImpl.class);

    private final DbProvisioningService dbProvisioningService;
    private final SecurityTenantService securityTenantService;
    private final StorageProvisioningService storageProvisioningService;


    public ProvisioningServiceImpl(DbProvisioningService dbProvisioningService, SecurityTenantService securityTenantService, StorageProvisioningService storageProvisioningService) {
        this.dbProvisioningService = dbProvisioningService;
        this.securityTenantService = securityTenantService;
        this.storageProvisioningService = storageProvisioningService;
    }

    @Override
    public Tenant provisionNewTenant(Tenant tenant, User user) throws ProvisioningException {
        try {
            String schemaName = tenant.getSchemaName();
            LOGGER.info("provision tenant schema {}.", schemaName);
            dbProvisioningService.createTenantSchema(schemaName);
        } catch (LiquibaseException e) {
            throw new ProvisioningException("provisioning failed.", e);
        }

        LOGGER.info("create client secret for new tenant {}.", tenant.getDomain());

        if(tenant.getClientSecret().isEmpty()) {
            String clientSecret = UUID.randomUUID().toString();
            tenant = ImmutableTenant.builder()
                    .from(tenant)
                    .clientSecret(clientSecret)
                    .build();
        }

        LOGGER.info("create security realm for new tenant {}.", tenant.getDomain());

        securityTenantService.createRealm(tenant, user);

        LOGGER.info("create object storage for new tenant {}.", tenant.getDomain());

        storageProvisioningService.provisionStorage(tenant);

        return tenant;
    }

    @Override
    public void createUser(Tenant tenant, User user) {
        securityTenantService.createUser(tenant, user);
    }
}
