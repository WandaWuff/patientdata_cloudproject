package de.htwg.konstanz.cad.provisioning.security.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.boot.context.properties.bind.DefaultValue;

@ConfigurationProperties(prefix = "provisioning.security")
@ConstructorBinding
public class ProvisioningSecurityProperties {

    private final String url;
    private final String masterRealm;
    private final String clientId;
    private final String clientSecret;

    private final String username;
    private final String password;

    public ProvisioningSecurityProperties (
            String url,
            @DefaultValue("master") String masterRealm,
            String clientId,
            String clientSecret,
            String username,
            String password
    ) {

        this.url = url;
        this.masterRealm = masterRealm;

        this.clientId = clientId;
        this.clientSecret = clientSecret;
        this.username = username;
        this.password = password;
    }

    public String getUrl() {
        return url;
    }

    public String getMasterRealm() {
        return masterRealm;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
