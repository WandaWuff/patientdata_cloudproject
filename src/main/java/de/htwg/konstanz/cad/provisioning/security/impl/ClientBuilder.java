package de.htwg.konstanz.cad.provisioning.security.impl;

import org.keycloak.representations.idm.ClientRepresentation;

import java.util.Collections;
import java.util.List;

public class ClientBuilder {

    List<ClientRepresentation> createClients(String clientSecret) {
        return List.of(
                createBackendClient(clientSecret),
                createFrontendClient()
        );
    }

    private ClientRepresentation createBackendClient(String clientSecret) {
        ClientRepresentation client = new ClientRepresentation();
        client.setClientId("patientdata-backend");
        client.setPublicClient(false);
        client.setSecret(clientSecret);
        client.setStandardFlowEnabled(false);
        client.setDirectAccessGrantsEnabled(true);
        return client;
    }

    private ClientRepresentation createFrontendClient() {
        ClientRepresentation client = new ClientRepresentation();
        client.setClientId("patientdata-frontend");
        client.setPublicClient(true);
        client.setStandardFlowEnabled(true);
        client.setDirectAccessGrantsEnabled(true);
        client.setWebOrigins(Collections.singletonList("*"));
        return client;
    }
}
