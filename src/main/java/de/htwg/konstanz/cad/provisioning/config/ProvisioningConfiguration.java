package de.htwg.konstanz.cad.provisioning.config;

import de.htwg.konstanz.cad.provisioning.api.ProvisioningService;
import de.htwg.konstanz.cad.provisioning.security.api.SecurityTenantService;
import de.htwg.konstanz.cad.provisioning.security.config.ProvisioningSecurityConfiguration;
import de.htwg.konstanz.cad.provisioning.storage.api.StorageProvisioningService;
import de.htwg.konstanz.cad.provisioning.storage.config.StorageProvisionConfiguration;
import de.htwg.konstanz.cad.util.provisioning.DbContext;
import de.htwg.konstanz.cad.provisioning.impl.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.List;

@Configuration
@Import({ProvisioningSecurityConfiguration.class, StorageProvisionConfiguration.class})
public class ProvisioningConfiguration {

    @Bean
    ProvisioningService provisioningService(DbProvisioningService dbProvisioningService, SecurityTenantService securityTenantService, StorageProvisioningService storageProvisioningService) {
        return new ProvisioningServiceImpl(dbProvisioningService, securityTenantService, storageProvisioningService);
    }

    @Bean
    DbProvisioningService dbProvisioningService(List<DbContext> dbContexts, TenantSchemaSupplier tenantSchemaSupplier, SchemaCreator schemaCreator) {
        return new DbProvisioningService(tenantSchemaSupplier, new LiquibaseHelper(), dbContexts, schemaCreator);
    }

    @Bean
    SchemaCreator schemaCreator() {
        return new SchemaCreator();
    }
}
