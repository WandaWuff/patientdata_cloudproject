package de.htwg.konstanz.cad.provisioning.security.impl;

import de.htwg.konstanz.cad.provisioning.security.api.ClientSecretRetriever;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.adapters.KeycloakDeploymentBuilder;
import org.keycloak.adapters.spi.HttpFacade.Request;
import org.keycloak.common.enums.SslRequired;
import org.keycloak.representations.adapters.config.AdapterConfig;

import java.util.Map;

public class KeycloakConfigResolver implements org.keycloak.adapters.KeycloakConfigResolver {

    private final String authServerUrl;
    private final String resource;
    private final ClientSecretRetriever clientSecretRetriever;

    public KeycloakConfigResolver(ClientSecretRetriever clientSecretRetriever, String authServerUrl, String resource) {
        this.clientSecretRetriever = clientSecretRetriever;
        this.authServerUrl = authServerUrl;
        this.resource = resource;
    }

    @Override
    public KeycloakDeployment resolve(Request request) {
        String tenant = request.getHeader("X-Tenant");
        if (tenant != null) {

            String clientSecret = clientSecretRetriever.getClientSecret(tenant).orElseThrow(IllegalStateException::new);

            AdapterConfig adapterConfig = new AdapterConfig();

            adapterConfig.setRealm(tenant);
            adapterConfig.setAuthServerUrl(authServerUrl);
            adapterConfig.setSslRequired(SslRequired.NONE.name());
            adapterConfig.setResource(resource);
            adapterConfig.setCredentials(Map.of("secret", clientSecret));
            adapterConfig.setUseResourceRoleMappings(false);
            adapterConfig.setCors(true);
            adapterConfig.setPrincipalAttribute("preferred_username");

            return KeycloakDeploymentBuilder.build(adapterConfig);
        }

        // TODO messy that we need a none existing deployment to trick spring to accept requests
        //  when no tenant is provided.
        //  maybe use master realm?

        AdapterConfig adapterConfig = new AdapterConfig();
        adapterConfig.setRealm("master");
        adapterConfig.setAuthServerUrl(authServerUrl);
        adapterConfig.setSslRequired("external");
        adapterConfig.setResource(resource);
        adapterConfig.setCors(true);

        return KeycloakDeploymentBuilder.build(adapterConfig);
    }
}
