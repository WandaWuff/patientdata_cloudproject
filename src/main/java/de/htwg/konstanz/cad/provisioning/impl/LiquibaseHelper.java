package de.htwg.konstanz.cad.provisioning.impl;

import liquibase.integration.spring.MultiTenantSpringLiquibase;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.core.io.ResourceLoader;

import javax.sql.DataSource;
import java.util.List;

public class LiquibaseHelper {

    private final static String PATH_TO_SCRIPT_TEMPLATE = "classpath:db/changelog/%s/db.changelog-master.yml";

    public SpringLiquibase getLiquibaseForDb(ResourceLoader resourceLoader, DataSource dataSource, String dbName) {
        SpringLiquibase springLiquibase = new SpringLiquibase();
        springLiquibase.setChangeLog(getChangeLogPath(dbName));
        springLiquibase.setDataSource(dataSource);
        springLiquibase.setResourceLoader(resourceLoader);
        return springLiquibase;
    }

    public SpringLiquibase getLiquibaseForDbAndCustomer(ResourceLoader resourceLoader, DataSource dataSource, String dbName, String customerSchema) {
        SpringLiquibase springLiquibase = getLiquibaseForDb(resourceLoader, dataSource, dbName);
        springLiquibase.setDefaultSchema(customerSchema);
        return springLiquibase;
    }

    public MultiTenantSpringLiquibase getLiquibaseForDbAndCustomers(ResourceLoader resourceLoader, DataSource dataSource, String dbName, List<String> customerSchemas) {
        MultiTenantSpringLiquibase springLiquibase = new MultiTenantSpringLiquibase();
        springLiquibase.setChangeLog(getChangeLogPath(dbName));
        springLiquibase.setDataSource(dataSource);
        springLiquibase.setSchemas(customerSchemas);
        springLiquibase.setResourceLoader(resourceLoader);
        return springLiquibase;
    }

    private String getChangeLogPath(String dbName) {
        return String.format(PATH_TO_SCRIPT_TEMPLATE, dbName);
    }
}
