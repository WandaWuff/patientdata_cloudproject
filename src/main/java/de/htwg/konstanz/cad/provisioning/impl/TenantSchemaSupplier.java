package de.htwg.konstanz.cad.provisioning.impl;

import java.util.List;

public interface TenantSchemaSupplier {
    List<String> getTenantSchemaNames() throws ProvisioningException;
}
