package de.htwg.konstanz.cad.provisioning.security.impl;

import de.htwg.konstanz.cad.provisioning.api.Role;
import de.htwg.konstanz.cad.provisioning.api.User;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.Collections;
import java.util.UUID;
import java.util.stream.Collectors;

public class UserBuilder {

    public UserRepresentation createUser(User user) {

        CredentialRepresentation credential = new CredentialRepresentation();
        credential.setType(CredentialRepresentation.PASSWORD);
        credential.setValue(user.getPassword());
        credential.setTemporary(false);

        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setEmail(user.getEmail());
        userRepresentation.setEmailVerified(true);
        userRepresentation.setEnabled(true);
        userRepresentation.setUsername(user.getEmail());
        userRepresentation.setFirstName(user.getFirstName());
        userRepresentation.setLastName(user.getLastName());
        userRepresentation.setCredentials(Collections.singletonList(credential));
        userRepresentation.setId(UUID.randomUUID().toString());
        userRepresentation.setRealmRoles(user.getRoles()
                .stream()
                .map(Role::getRoleName)
                .collect(Collectors.toList())
        );

        return userRepresentation;
    }
}
