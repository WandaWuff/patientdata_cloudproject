package de.htwg.konstanz.cad.provisioning.security.api;

import de.htwg.konstanz.cad.provisioning.api.User;
import de.htwg.konstanz.cad.tenant.api.model.Tenant;

public interface SecurityTenantService {
    void createRealm(Tenant domain, User user);

    void createUser(Tenant tenant, User user);
}
