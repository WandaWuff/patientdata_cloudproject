package de.htwg.konstanz.cad.provisioning.config;

import de.htwg.konstanz.cad.provisioning.impl.TenantSchemaSupplier;
import de.htwg.konstanz.cad.provisioning.security.api.ClientSecretRetriever;
import de.htwg.konstanz.cad.util.tenant.TenantStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;

import java.util.Collections;
import java.util.Optional;

@Configuration
@Import(ProvisioningConfiguration.class)
public class IntegrationTestProvisioningConfiguration {

    public static final String INTEGRATION_CUSTOMER_SCHEMA = "integration_com";

    @Bean
    TenantSchemaSupplier customerSchemaSupplier() {
        return () -> Collections.singletonList(INTEGRATION_CUSTOMER_SCHEMA);
    }

    @Bean
    @Primary
    TenantStore tenantStore(){
        TenantStore tenantStore = new TenantStore();
        tenantStore.setTenantId(INTEGRATION_CUSTOMER_SCHEMA);
        return tenantStore;
    }

    @Bean
    ClientSecretRetriever clientSecretRetriever() {
        return tenant -> Optional.of("");
    }
}
