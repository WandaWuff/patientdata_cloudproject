package de.htwg.konstanz.cad.provisioning.impl;

public class ProvisioningException extends Exception {

    private static final long serialVersionUID = 1L;

    public ProvisioningException() {
    }

    public ProvisioningException(String message) {
        super(message);
    }

    public ProvisioningException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProvisioningException(Throwable cause) {
        super(cause);
    }
}
