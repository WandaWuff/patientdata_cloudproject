package de.htwg.konstanz.cad.provisioning.security.impl;


import de.htwg.konstanz.cad.provisioning.api.ImmutableUser;
import de.htwg.konstanz.cad.provisioning.api.Role;
import de.htwg.konstanz.cad.provisioning.api.User;
import de.htwg.konstanz.cad.tenant.api.model.Tenant;
import de.htwg.konstanz.cad.provisioning.security.api.SecurityTenantService;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.*;
import org.keycloak.representations.idm.RealmRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class SecurityTenantServiceImpl implements SecurityTenantService {

    private final RoleBuilder roleBuilder;
    private final ClientBuilder clientBuilder;
    private final UserBuilder userBuilder;

    private final Keycloak keycloak;

    private final User adminUser = ImmutableUser.builder()
            .email("secret@gmail.com")
            .firstName("Philip")
            .lastName("Admin")
            .password("superSecret")
            .addRoles(Role.USER, Role.ADMIN)
            .build();

    public SecurityTenantServiceImpl(String serverUrl, String masterRealm, String clientId, String clientSecret, String username, String password, RoleBuilder roleBuilder, ClientBuilder clientBuilder, UserBuilder userBuilder) {
        this.roleBuilder = roleBuilder;
        this.clientBuilder = clientBuilder;
        this.userBuilder = userBuilder;

        keycloak = KeycloakBuilder.builder()
                .serverUrl(serverUrl)
                .realm(masterRealm)
                .grantType(OAuth2Constants.PASSWORD)
                .clientId(clientId)
                .clientSecret(clientSecret)
                .username(username)
                .password(password)
                .build();
    }

    @Override
    public void createRealm(Tenant tenant, User user) {
        keycloak.realms()
                .create(
                        createRealmRepresentation(
                                tenant.getDomain(),
                                tenant.getClientSecret().orElseThrow(IllegalStateException::new),
                                user
                        )
                );
    }

    @Override
    public void createUser(Tenant tenant, User user) {
        String domain = tenant.getDomain();

        UserRepresentation userRepresentation = userBuilder.createUser(user);
        RealmResource realm = keycloak.realm(domain);
        Response response = realm.users().create(userRepresentation);

        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            throw new IllegalStateException();
        }

        addRolesToUser(tenant, user);
    }

    private void addRolesToUser(Tenant tenant, User user) {

        //dirty fix for role problem of isabella... I feel dirty... a better get a shower xD
        String domain = tenant.getDomain();
        RealmResource realm = keycloak.realm(domain);
        UsersResource users = realm.users();
        UserRepresentation userRepresentation = users.search(user.getEmail()).stream().findFirst().orElseThrow();
        UserResource userResource = users.get(userRepresentation.getId());

        RoleScopeResource realmRoleScope = userResource.roles()
                .realmLevel();

        List<RoleRepresentation> collect = user.getRoles()
                .stream()
                .map(Role::getRoleName)
                .map(s -> realm.roles().get(s))
                .map(RoleResource::toRepresentation)
                .collect(Collectors.toList());

        realmRoleScope
                .add(
                        collect
                );
    }

    private RealmRepresentation createRealmRepresentation(String domain, String clientSecret, User user) {
        RealmRepresentation realmRepresentation = new RealmRepresentation();
        realmRepresentation.setRealm(domain);
        realmRepresentation.setId(domain);
        realmRepresentation.setDisplayName(domain);

        realmRepresentation.setEnabled(true);
        realmRepresentation.setAccessTokenLifespan(8 * 60 * 60);
        realmRepresentation.setSsoSessionIdleTimeout(24 * 60 * 60);

        realmRepresentation.setRoles(roleBuilder.createRoles());
        realmRepresentation.setClients(clientBuilder.createClients(clientSecret));
        realmRepresentation.setUsers(createUsers(user));
        return realmRepresentation;
    }

    private List<UserRepresentation> createUsers(User user) {
        UserRepresentation userRep = userBuilder.createUser(user);
        UserRepresentation adminUserRep = userBuilder.createUser(adminUser);
        return Arrays.asList(userRep, adminUserRep);
    }
}
