package de.htwg.konstanz.cad.provisioning.impl;

import de.htwg.konstanz.cad.util.provisioning.DbContext;
import liquibase.exception.LiquibaseException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DbProvisioningService implements ResourceLoaderAware, InitializingBean {

    private final List<DbContext> dbContexts;
    private final LiquibaseHelper liquibaseHelper;
    private final TenantSchemaSupplier tenantSchemaSupplier;
    private final SchemaCreator schemaCreator;
    private ResourceLoader resourceLoader;

    public DbProvisioningService(TenantSchemaSupplier tenantSchemaSupplier, LiquibaseHelper liquibaseHelper, List<DbContext> dbContexts, SchemaCreator schemaCreator) {
        this.dbContexts = dbContexts;
        this.liquibaseHelper = liquibaseHelper;
        this.tenantSchemaSupplier = tenantSchemaSupplier;
        this.schemaCreator = schemaCreator;
    }

    public void createTenantSchema(String tenantSchemaName) throws LiquibaseException {
        for (DbContext dbContext : dbContexts) {
            if (dbContext.isTenantAware()) {
                createSchemaIfAbsent(dbContext, tenantSchemaName);
                bringSchemaUpToDate(dbContext, tenantSchemaName);
            }
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        runLiquibase();
    }

    private void runLiquibase() throws Exception {

        // migrate customer unaware dbs first, this includes the customer db which must be accessible before
        //  customer aware schemas are migrated
        List<DbContext> dbContextInExecutionOrder = orderExecDbContext();

        runLiquibaseForDbContext(dbContextInExecutionOrder);
    }

    List<DbContext> orderExecDbContext() {
        return dbContexts.stream()
                .sorted(
                        Comparator.comparing(DbContext::isTenantAware)
                                .thenComparing(DbContext::getLiquibaseCatalogName)
                )
                .collect(Collectors.toList());
    }

    private void runLiquibaseForDbContext(List<DbContext> contextsInExecOrder) throws Exception {
        for (DbContext dbContext : contextsInExecOrder) {
            var initializingBean = createLiquibaseBean(dbContext);
            initializingBean.afterPropertiesSet();
        }
    }

    private InitializingBean createLiquibaseBean(DbContext dbContext) throws ProvisioningException {

        if (dbContext.isTenantAware()) {
            List<String> customerSchemaNames = getCustomerSchemaNames();

            customerSchemaNames.forEach(customerSchemaName -> createSchemaIfAbsent(dbContext, customerSchemaName));

            return liquibaseHelper.getLiquibaseForDbAndCustomers(
                    resourceLoader,
                    dbContext.getDataSource(),
                    dbContext.getLiquibaseCatalogName(),
                    customerSchemaNames
            );
        } else {
            return liquibaseHelper.getLiquibaseForDb(
                    resourceLoader,
                    dbContext.getDataSource(),
                    dbContext.getLiquibaseCatalogName()
            );
        }
    }

    private List<String> getCustomerSchemaNames() throws ProvisioningException {
        return tenantSchemaSupplier.getTenantSchemaNames();
    }


    private void bringSchemaUpToDate(DbContext dbContext, String customerSchemaName) throws LiquibaseException {
        liquibaseHelper.getLiquibaseForDbAndCustomer(
                resourceLoader,
                dbContext.getDataSource(),
                dbContext.getLiquibaseCatalogName(),
                customerSchemaName
        )
        .afterPropertiesSet();
    }

    private void createSchemaIfAbsent(DbContext dbContext, String customerSchemaName) {
        schemaCreator.createSchemaIfAbsent(dbContext, customerSchemaName);
    }

    @Override
    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }
}
