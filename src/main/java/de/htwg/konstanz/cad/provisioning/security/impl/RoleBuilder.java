package de.htwg.konstanz.cad.provisioning.security.impl;

import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.RolesRepresentation;

import java.util.List;
import java.util.UUID;

public class RoleBuilder {
    public RoleBuilder() {
    }

    RolesRepresentation createRoles() {
        RolesRepresentation rolesRepresentation = new RolesRepresentation();
        rolesRepresentation.setRealm(
                List.of(
                        createUserRole(),
                        createAdminRole()
                ));
        return rolesRepresentation;
    }

    RoleRepresentation createAdminRole() {
        RoleRepresentation roleAdmin = new RoleRepresentation();
        roleAdmin.setId(UUID.randomUUID().toString());
        roleAdmin.setName("admin");
        roleAdmin.setDescription("A Role for Admin Users");
        return roleAdmin;
    }

    RoleRepresentation createUserRole() {
        RoleRepresentation roleAdmin = new RoleRepresentation();
        roleAdmin.setId(UUID.randomUUID().toString());
        roleAdmin.setName("user");
        roleAdmin.setDescription("A Role Default Role, which enables a User to login");
        return roleAdmin;
    }
}
