package de.htwg.konstanz.cad.provisioning.security.api;

import java.util.Optional;

public interface ClientSecretRetriever {
    Optional<String> getClientSecret(String tenant);
}
