package de.htwg.konstanz.cad.provisioning.storage.api;

import de.htwg.konstanz.cad.tenant.api.model.Tenant;

public interface StorageProvisioningService {

    void provisionStorage(Tenant tenant);
}
