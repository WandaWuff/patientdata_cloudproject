package de.htwg.konstanz.cad.provisioning.storage.impl;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CreateBucketRequest;
import de.htwg.konstanz.cad.provisioning.storage.api.StorageProvisioningService;
import de.htwg.konstanz.cad.tenant.api.model.Tenant;
import software.amazon.awssdk.core.exception.SdkClientException;

import static de.htwg.konstanz.cad.s3.S3Utils.emptyS3Bucket;


public class S3ProvisioningServiceImpl implements StorageProvisioningService {

    private final AmazonS3 amazonS3;

    public S3ProvisioningServiceImpl(AmazonS3 amazonS3) {
        this.amazonS3 = amazonS3;
    }

    @Override
    public void provisionStorage(Tenant tenant) {
        String storageName = tenant.getStorageName();
        if (!amazonS3.doesBucketExistV2(storageName)) {
            // Because the CreateBucketRequest object doesn't specify a region, the
            // bucket is created in the region specified in the client.
            amazonS3.createBucket(new CreateBucketRequest(storageName));
        }
    }

    public void deleteS3Bucket(String bucketName) {
        try {
            emptyS3Bucket(amazonS3, bucketName);
            amazonS3.deleteBucket(bucketName);
        } catch (AmazonServiceException | SdkClientException e) {
            throw new IllegalStateException(e);
        }
    }
}
