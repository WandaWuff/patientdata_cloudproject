package de.htwg.konstanz.cad.s3;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import software.amazon.awssdk.core.exception.SdkClientException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class S3Utils {

    /**
     * Uploads object and its metadata to S3 bucket.
     * @param clientRegion client's region
     * @param bucketName name of bucket
     * @param objectKeyName file object key name
     * @param fileName path to file to upload
     * @param title file netadata: title
     * @param author file netadata: author
     * @param description file netadata: description
     * @param date file netadata: date
     * @param credentials client's credentials
     */
    public static void uploadObject(String clientRegion, String bucketName,
                                    String objectKeyName, String fileName,
                                    String title, String author, String description,
                                    String date, ProfileCredentialsProvider credentials) {
        try {
            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withRegion(clientRegion)
                    .withCredentials(credentials)
                    .build();

            // Upload a file as a new object with ContentType and title specified.
            PutObjectRequest request = new PutObjectRequest(bucketName, objectKeyName, new File(fileName));
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("plain/text");
            metadata.addUserMetadata("title", title);
            metadata.addUserMetadata("author", author);
            metadata.addUserMetadata("description", description);
            metadata.addUserMetadata("date", date);
            request.setMetadata(metadata);
            s3Client.putObject(request);
        } catch (AmazonServiceException | SdkClientException e) {
            e.printStackTrace();
        }
    }



    public static void deleteObject(String clientRegion, String bucketName,
                                    ProfileCredentialsProvider credentials,
                                    String objectKey) {
        try {
            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withCredentials(credentials)
                    .withRegion(clientRegion)
                    .build();

            s3Client.deleteObject(bucketName, objectKey);
        } catch (AmazonServiceException | SdkClientException e) {
            e.printStackTrace();
        }
    }

    public static void emptyS3Bucket(AmazonS3 amazonS3, String bucketName) {
        try {

            ObjectListing objectListing = amazonS3.listObjects(bucketName);
            while (true) {
                Iterator<S3ObjectSummary> objIter = objectListing.getObjectSummaries().iterator();
                while (objIter.hasNext()) {
                    amazonS3.deleteObject(bucketName, objIter.next().getKey());
                }

                // If the bucket contains many objects, the listObjects() call
                // might not return all of the objects in the first listing. Check to
                // see whether the listing was truncated.
                // If so, retrieve the next page of objects and delete them.
                if (objectListing.isTruncated()) {
                    objectListing = amazonS3.listNextBatchOfObjects(objectListing);
                } else {
                    break;
                }
            }
        } catch (AmazonServiceException | SdkClientException e) {
            e.printStackTrace();
        }
    }

}
