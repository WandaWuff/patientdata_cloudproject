package de.htwg.konstanz.cad.rs.patientdata.impl;

import de.htwg.konstanz.cad.patientdata.api.model.EmergencyContact;
import de.htwg.konstanz.cad.patientdata.api.model.Patient;
import de.htwg.konstanz.cad.patientdata.api.service.PatientDataService;
import de.htwg.konstanz.cad.rs.patientdata.api.EmergencyContactController;

import java.util.List;

public class EmergencyContactControllerImpl implements EmergencyContactController {

    private final PatientDataService patientDataService;

    public EmergencyContactControllerImpl(PatientDataService patientDataService) {
        this.patientDataService = patientDataService;
    }

    @Override
    public void save(EmergencyContact emergencyContact) {
        patientDataService.save(emergencyContact);
    }

    @Override
    public List<EmergencyContact> getAll() {
        return patientDataService.findAllEmergencyContacts();
    }
}
