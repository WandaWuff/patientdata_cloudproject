package de.htwg.konstanz.cad.rs.tenant.api;

import de.htwg.konstanz.cad.provisioning.api.User;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public interface UserController {

    @PostMapping
    @ResponseStatus( HttpStatus.CREATED )
    void createUser(@RequestBody User user);
}
