package de.htwg.konstanz.cad.rs.tenant.impl;

import de.htwg.konstanz.cad.provisioning.api.User;
import de.htwg.konstanz.cad.tenant.api.model.Tenant;
import de.htwg.konstanz.cad.tenant.api.service.TenantService;
import de.htwg.konstanz.cad.provisioning.impl.ProvisioningException;
import de.htwg.konstanz.cad.provisioning.api.ProvisioningService;
import de.htwg.konstanz.cad.rs.tenant.api.TenantController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class TenantControllerImpl implements TenantController {

    private final Logger LOGGER = LogManager.getLogger(TenantControllerImpl.class);
    private final TenantService tenantService;
    private final ProvisioningService provisioningService;

    public TenantControllerImpl(TenantService tenantService, ProvisioningService provisioningService) {
        this.tenantService = tenantService;
        this.provisioningService = provisioningService;
    }

    @Override
    public void createTenant(Tenant tenant, User user) {
        LOGGER.info("New tenant received {}", tenant);

        String domain = tenant.getDomain();
        if (tenantService.exists(domain)) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "there is already a tenant with the domain " + domain);
        }

        try {
            tenant = provisioningService.provisionNewTenant(tenant,user);
        } catch (ProvisioningException e) {
            LOGGER.warn("Provisioning of tenant failed. tenant={}.", tenant, e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Provisioning of tenant failed " + domain);
        }

        LOGGER.info("New tenant provisioned {}", tenant);

        tenantService.save(tenant);

        LOGGER.info("New tenant saved {}", tenant);
    }


}
