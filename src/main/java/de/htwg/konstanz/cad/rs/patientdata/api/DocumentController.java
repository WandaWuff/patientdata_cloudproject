package de.htwg.konstanz.cad.rs.patientdata.api;

import de.htwg.konstanz.cad.patientdata.api.model.Document;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/documents")
public interface DocumentController {

    @PutMapping
    void save(@RequestBody Document document);

    @GetMapping
    List<Document> getAllDocuments();

    @PutMapping("/{id}/file")
    void saveFile(@PathVariable("id") UUID id, @RequestParam("file") MultipartFile file) throws IOException;

    @GetMapping("/{id}/file")
    ResponseEntity<byte[]> findFile(@PathVariable("id") UUID id, HttpServletRequest request) throws IOException;
}
