package de.htwg.konstanz.cad.rs.patientdata.impl;

import de.htwg.konstanz.cad.patientdata.api.model.Patient;
import de.htwg.konstanz.cad.patientdata.api.service.PatientDataService;
import de.htwg.konstanz.cad.rs.patientdata.api.PatientController;

public class PatientControllerImpl implements PatientController {

    private final PatientDataService patientDataService;

    public PatientControllerImpl(PatientDataService patientDataService) {
        this.patientDataService = patientDataService;
    }

    @Override
    public void save(Patient patient) {
        patientDataService.save(patient);
    }

    @Override
    public Patient getPatientData() {
        return patientDataService.getPatientData();
    }
}
