package de.htwg.konstanz.cad.rs.patientdata.impl;

import de.htwg.konstanz.cad.patientdata.api.model.Diagnosis;
import de.htwg.konstanz.cad.patientdata.api.model.Document;
import de.htwg.konstanz.cad.patientdata.api.service.PatientDataService;
import de.htwg.konstanz.cad.rs.patientdata.api.DiagnosisController;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class DiagnosisControllerImpl implements DiagnosisController {

    private final PatientDataService patientDataService;

    public DiagnosisControllerImpl(PatientDataService patientDataService) {
        this.patientDataService = patientDataService;
    }

    @Override
    public void save(Document document, UUID diagnosis) {
        patientDataService.save(document, Optional.of(diagnosis));
    }

    @Override
    public List<Document> getAllDocumentsForDiagnosis(UUID diagnosis) {
        return patientDataService.findAllDocumentsForDiagnosis(diagnosis);
    }

    @Override
    public void save(Diagnosis diagnosis) {
        patientDataService.save(diagnosis);
    }

    @Override
    public List<Diagnosis> getAllDiagnoses() {
        return patientDataService.findAllDiagnosis();
    }
}
