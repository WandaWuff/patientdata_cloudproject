package de.htwg.konstanz.cad.rs.patientdata.api;

import de.htwg.konstanz.cad.patientdata.api.model.EmergencyContact;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/emergencyContacts")
public interface EmergencyContactController {

    @PutMapping
    void save(@RequestBody EmergencyContact emergencyContact);

    @GetMapping
    List<EmergencyContact> getAll();
}
