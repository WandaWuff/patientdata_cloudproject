package de.htwg.konstanz.cad.rs.patientdata.impl;

import de.htwg.konstanz.cad.patientdata.api.model.Document;
import de.htwg.konstanz.cad.patientdata.api.service.PatientDataService;
import de.htwg.konstanz.cad.rs.patientdata.api.DocumentController;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public class DocumentControllerImpl implements DocumentController {

    private final PatientDataService patientDataService;

    public DocumentControllerImpl(PatientDataService patientDataService) {
        this.patientDataService = patientDataService;
    }

    @Override
    public void save(Document document) {
        patientDataService.save(document, Optional.empty());
    }

    @Override
    public List<Document> getAllDocuments() {
        return patientDataService.findAllDocuments();
    }

    @Override
    public void saveFile(UUID id, MultipartFile file) throws IOException {
        patientDataService.save(id, file.getInputStream());
    }

    @Override
    public ResponseEntity<byte[]> findFile(UUID id, HttpServletRequest request) throws IOException {
        // TODO ugly hack in order to not implement sql ...
        Document document = patientDataService.findAllDocuments()
                .stream()
                .filter(doc -> doc.getId().equals(id))
                .findFirst()
                .orElseThrow();

        String filename = document.getTitle();
        String contentType = Optional.ofNullable(request.getServletContext().getMimeType(filename))
                .orElse("application/octet-stream");

        byte[] file = patientDataService.getFile(id);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, String.format("attachment; filename=\"%s\"", filename))
                .body(file);
    }
}
