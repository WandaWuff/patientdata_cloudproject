package de.htwg.konstanz.cad.rs.tenant.api;

import de.htwg.konstanz.cad.provisioning.api.User;
import de.htwg.konstanz.cad.tenant.api.model.Tenant;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tenants")
public interface TenantController {

    void createTenant(Tenant tenant, User user);
}
