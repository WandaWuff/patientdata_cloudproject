package de.htwg.konstanz.cad.rs.patientdata.config;

import de.htwg.konstanz.cad.patientdata.api.service.PatientDataService;
import de.htwg.konstanz.cad.patientdata.config.PatientDataConfiguration;
import de.htwg.konstanz.cad.rs.patientdata.api.DiagnosisController;
import de.htwg.konstanz.cad.rs.patientdata.api.DocumentController;
import de.htwg.konstanz.cad.rs.patientdata.api.EmergencyContactController;
import de.htwg.konstanz.cad.rs.patientdata.api.PatientController;
import de.htwg.konstanz.cad.rs.patientdata.impl.DiagnosisControllerImpl;
import de.htwg.konstanz.cad.rs.patientdata.impl.DocumentControllerImpl;
import de.htwg.konstanz.cad.rs.patientdata.impl.EmergencyContactControllerImpl;
import de.htwg.konstanz.cad.rs.patientdata.impl.PatientControllerImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(PatientDataConfiguration.class)
public class RsPatientDataConfiguration {

    @Bean
    DiagnosisController diagnosisController(PatientDataService patientDataService){
        return new DiagnosisControllerImpl(patientDataService);
    }

    @Bean
    DocumentController documentController(PatientDataService patientDataService){
        return new DocumentControllerImpl(patientDataService);
    }

    @Bean
    EmergencyContactController emergencyContactController(PatientDataService patientDataService){
        return new EmergencyContactControllerImpl(patientDataService);
    }

    @Bean
    PatientController patientController(PatientDataService patientDataService){
        return new PatientControllerImpl(patientDataService);
    }
}
