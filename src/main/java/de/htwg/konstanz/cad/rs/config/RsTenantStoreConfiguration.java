package de.htwg.konstanz.cad.rs.config;

import de.htwg.konstanz.cad.tenant.api.service.TenantService;
import de.htwg.konstanz.cad.util.tenant.TenantAwareRestController;
import de.htwg.konstanz.cad.util.tenant.TenantStore;
import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.aop.target.ThreadLocalTargetSource;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.Filter;
import java.util.*;
import java.util.stream.Collectors;

@Configuration
public class RsTenantStoreConfiguration {

    @Bean
    public Filter tenantFilter(TenantStore tenantStore, TenantService tenantService) {
        return new TenantFilter(tenantStore, tenantService);
    }

    @Bean
    public FilterRegistrationBean<Filter> tenantFilterRegistration(Optional<List<TenantAwareRestController>> optRestControllers) {

        List<String> urlPatterns = optRestControllers.stream()
                .flatMap(Collection::stream)
                .map(TenantAwareRestController::getClass)
                .map(aClass -> aClass.getAnnotation(RequestMapping.class))
                .filter(Objects::nonNull)
                .map(RequestMapping::value)
                .flatMap(Arrays::stream)
                .map(path -> String.format("%s/*", path))
                .collect(Collectors.toList());

        FilterRegistrationBean<Filter> result = new FilterRegistrationBean<>();
        result.setFilter(this.tenantFilter(null, null));
        result.setUrlPatterns(urlPatterns);
        result.setName("Tenant Store Filter");
        result.setOrder(1);
        return result;
    }

    @Bean(destroyMethod = "destroy")
    public ThreadLocalTargetSource threadLocalTenantStore() {
        ThreadLocalTargetSource result = new ThreadLocalTargetSource();
        result.setTargetBeanName("tenantStore");
        return result;
    }

    @Primary
    @Bean(name = "proxiedThreadLocalTargetSource")
    public ProxyFactoryBean proxiedThreadLocalTargetSource(ThreadLocalTargetSource threadLocalTargetSource) {
        ProxyFactoryBean result = new ProxyFactoryBean();
        result.setTargetSource(threadLocalTargetSource);
        return result;
    }

    @Bean(name = "tenantStore")
    @Scope(scopeName = "prototype")
    public TenantStore tenantStore() {
        return new TenantStore();
    }
}
