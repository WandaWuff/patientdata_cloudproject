package de.htwg.konstanz.cad.rs.tenant.impl;

import de.htwg.konstanz.cad.provisioning.api.User;
import de.htwg.konstanz.cad.rs.patientdata.api.PatientController;
import de.htwg.konstanz.cad.rs.tenant.api.Registration;
import de.htwg.konstanz.cad.rs.tenant.api.RegistrationController;
import de.htwg.konstanz.cad.rs.tenant.api.TenantController;
import de.htwg.konstanz.cad.rs.tenant.api.UserController;
import de.htwg.konstanz.cad.tenant.api.model.Tenant;
import de.htwg.konstanz.cad.util.tenant.TenantStore;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RegistrationControllerImpl implements RegistrationController {

    private final Logger LOGGER = LogManager.getLogger(RegistrationControllerImpl.class);
    private final TenantController tenantController;
    private final PatientController patientController;
    private final TenantStore tenantStore;

    public RegistrationControllerImpl(TenantController tenantController, PatientController patientController, TenantStore tenantStore) {
        this.tenantController = tenantController;
        this.patientController = patientController;
        this.tenantStore = tenantStore;
    }

    @Override
    public void registerNewTenant(Registration registration) {
        Tenant tenant = registration.getTenant();
        User user = registration.getUser();
        tenantController.createTenant(tenant, user);
        try {
            tenantStore.setTenantId(tenant.getDomain());
            tenantStore.setSchemaName(tenant.getSchemaName());

            patientController.save(registration.getPatient());

        }finally {
            tenantStore.clear();
        }
    }
}
