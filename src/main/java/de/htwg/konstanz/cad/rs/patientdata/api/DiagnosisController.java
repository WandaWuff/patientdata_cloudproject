package de.htwg.konstanz.cad.rs.patientdata.api;

import de.htwg.konstanz.cad.patientdata.api.model.Diagnosis;
import de.htwg.konstanz.cad.patientdata.api.model.Document;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/diagnoses")
public interface DiagnosisController {

    @GetMapping
    List<Diagnosis> getAllDiagnoses();

    @PutMapping
    void save(@RequestBody Diagnosis diagnosis);

    @GetMapping(value = "/{id}/documents")
    List<Document> getAllDocumentsForDiagnosis(@PathVariable("id") UUID diagnosis);

    @PutMapping(value = "/{id}/documents")
    void save(@RequestBody Document document, @PathVariable("id") UUID diagnosis);
}
