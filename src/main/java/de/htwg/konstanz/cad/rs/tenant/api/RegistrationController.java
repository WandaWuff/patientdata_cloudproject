package de.htwg.konstanz.cad.rs.tenant.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/register")
public interface RegistrationController {

    @PostMapping
    @ResponseStatus( HttpStatus.CREATED )
    void registerNewTenant(@RequestBody Registration registration);
}
