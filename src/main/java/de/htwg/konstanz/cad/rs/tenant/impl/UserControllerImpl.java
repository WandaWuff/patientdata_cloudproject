package de.htwg.konstanz.cad.rs.tenant.impl;

import de.htwg.konstanz.cad.provisioning.api.ProvisioningService;
import de.htwg.konstanz.cad.provisioning.api.User;
import de.htwg.konstanz.cad.rs.tenant.api.UserController;
import de.htwg.konstanz.cad.tenant.api.model.Tenant;
import de.htwg.konstanz.cad.tenant.api.service.TenantService;
import de.htwg.konstanz.cad.util.tenant.TenantStore;

public class UserControllerImpl implements UserController {

    private final TenantService tenantService;
    private final ProvisioningService provisioningService;
    private final TenantStore tenantStore;

    public UserControllerImpl(TenantService tenantService, ProvisioningService provisioningService, TenantStore tenantStore) {
        this.tenantService = tenantService;
        this.provisioningService = provisioningService;
        this.tenantStore = tenantStore;
    }

    @Override
    public void createUser(User user) {
        String tenantId = tenantStore.getTenantId().orElseThrow(IllegalStateException::new);
        Tenant tenant = tenantService.get(tenantId);
        provisioningService.createUser(tenant, user);
    }
}
