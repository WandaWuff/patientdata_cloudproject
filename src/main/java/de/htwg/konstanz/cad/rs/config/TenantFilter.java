package de.htwg.konstanz.cad.rs.config;

import com.google.common.base.Strings;
import de.htwg.konstanz.cad.tenant.api.model.Tenant;
import de.htwg.konstanz.cad.tenant.api.service.TenantService;
import de.htwg.konstanz.cad.util.tenant.TenantStore;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class TenantFilter implements javax.servlet.Filter {

    private final static String TENANT_HEADER_NAME = "X-Tenant";

    private final TenantStore tenantStore;
    private final TenantService tenantService;

    public TenantFilter(TenantStore tenantStore, TenantService tenantService) {
        this.tenantStore = tenantStore;
        this.tenantService = tenantService;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        var request = (HttpServletRequest) servletRequest;
        String tenantId = request.getHeader(TENANT_HEADER_NAME);
        try {
            if(!Strings.isNullOrEmpty(tenantId)) {
                Tenant tenant = tenantService.get(tenantId);
                this.tenantStore.setTenantId(tenant.getDomain());
                this.tenantStore.setSchemaName(tenant.getSchemaName());
            }
            chain.doFilter(servletRequest, servletResponse);
        } finally {
            // Otherwise when a previously used container thread is used, it will have the old tenant id set and
            // if for some reason this filter is skipped, tenantStore will hold an unreliable value
            this.tenantStore.clear();
        }
    }
}
