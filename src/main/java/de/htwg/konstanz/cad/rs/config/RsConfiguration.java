package de.htwg.konstanz.cad.rs.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.htwg.konstanz.cad.patientdata.config.PatientDataConfiguration;
import de.htwg.konstanz.cad.rs.patientdata.config.RsPatientDataConfiguration;
import de.htwg.konstanz.cad.rs.tenant.config.RsTenantConfiguration;
import de.htwg.konstanz.cad.util.json.JsonHelper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@Import({
        RsTenantStoreConfiguration.class,
        RsTenantConfiguration.class,
        SecurityConfiguration.class,
        RsPatientDataConfiguration.class,
        AwsConfiguration.class
})
public class RsConfiguration {

    @Bean
    @Primary
    public ObjectMapper objectMapper(){
        return new JsonHelper().newObjectMapper();
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }
        };
    }
}
