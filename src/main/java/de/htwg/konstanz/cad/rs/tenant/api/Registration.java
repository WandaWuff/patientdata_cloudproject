package de.htwg.konstanz.cad.rs.tenant.api;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import de.htwg.konstanz.cad.patientdata.api.model.Patient;
import de.htwg.konstanz.cad.provisioning.api.User;
import de.htwg.konstanz.cad.tenant.api.model.Tenant;
import org.immutables.value.Value;

@Value.Immutable
@JsonSerialize(as = ImmutableRegistration.class)
@JsonDeserialize(as = ImmutableRegistration.class)
public interface Registration {

    Tenant getTenant();
    User getUser();
    Patient getPatient();
}
