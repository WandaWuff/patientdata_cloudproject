package de.htwg.konstanz.cad.rs.tenant.config;

import de.htwg.konstanz.cad.provisioning.config.ProvisioningConfiguration;
import de.htwg.konstanz.cad.rs.patientdata.api.PatientController;
import de.htwg.konstanz.cad.rs.tenant.api.RegistrationController;
import de.htwg.konstanz.cad.rs.tenant.api.UserController;
import de.htwg.konstanz.cad.rs.tenant.impl.RegistrationControllerImpl;
import de.htwg.konstanz.cad.rs.tenant.impl.UserControllerImpl;
import de.htwg.konstanz.cad.tenant.api.model.Tenant;
import de.htwg.konstanz.cad.tenant.api.service.TenantService;
import de.htwg.konstanz.cad.tenant.config.TenantConfiguration;
import de.htwg.konstanz.cad.provisioning.impl.TenantSchemaSupplier;
import de.htwg.konstanz.cad.provisioning.impl.ProvisioningException;
import de.htwg.konstanz.cad.provisioning.api.ProvisioningService;
import de.htwg.konstanz.cad.provisioning.security.api.ClientSecretRetriever;
import de.htwg.konstanz.cad.rs.tenant.api.TenantController;
import de.htwg.konstanz.cad.rs.tenant.impl.TenantControllerImpl;
import de.htwg.konstanz.cad.util.tenant.TenantStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Configuration
@Import({TenantConfiguration.class, ProvisioningConfiguration.class})
public class RsTenantConfiguration {

    @Bean
    public ClientSecretRetriever clientSecretRetriever(TenantService tenantService) {

        Map<String, String> secretCache = new ConcurrentHashMap<>();
        return tenant -> {
            try {
                String clientSecret = secretCache.computeIfAbsent(tenant, domain -> {
                    Tenant customer = tenantService.get(domain);
                    return customer.getClientSecret()
                            .orElseThrow(NoSuchElementException::new);
                });
                return Optional.of(clientSecret);
            } catch (RuntimeException e) {
                return Optional.empty();
            }
        };
    }

    @Bean
    TenantSchemaSupplier customerSchemaSupplier(TenantService tenantService) {
        return () -> {
            try {
                return tenantService.getAllSchemaNames();
            } catch (RuntimeException e) {
                throw new ProvisioningException(e);
            }
        };
    }

    @Bean
    public TenantController tenantController(TenantService tenantService, ProvisioningService provisioningService) {
        return new TenantControllerImpl(tenantService, provisioningService);
    }

    @Bean
    public UserController userController(TenantService tenantService, ProvisioningService provisioningService, TenantStore tenantStore) {
        return new UserControllerImpl(tenantService, provisioningService, tenantStore);
    }

    @Bean
    public RegistrationController registrationController(TenantStore tenantStore, PatientController patientController, TenantController tenantController){
        return new RegistrationControllerImpl(tenantController, patientController, tenantStore);
    }
}
