package de.htwg.konstanz.cad.rs.patientdata.api;

import de.htwg.konstanz.cad.patientdata.api.model.Patient;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/patient")
public interface PatientController {

    @PutMapping
    void save(@RequestBody Patient patient);

    @GetMapping
    Patient getPatientData();
}
