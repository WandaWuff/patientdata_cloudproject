#!/usr/bin/bash

echo start my init script

function is_keycloak_running() {
  local http_code
  http_code=$(curl -s -o /dev/null -w "%{http_code}" http://localhost:8080/auth/admin/realms)
  if [[ $http_code -eq 401 ]]; then
    return 0
  else
    return 1
  fi
}

function addClientIfNotPresent() {

  until is_keycloak_running; do
    echo Keycloak is not running, waiting 5 seconds
    sleep 5
  done

  echo try to login

  ./opt/jboss/keycloak/bin/kcadm.sh config credentials --server http://localhost:8080/auth --realm master --user "$KEYCLOAK_USER" --password "$KEYCLOAK_PASSWORD"

  clients=$(./opt/jboss/keycloak/bin/kcadm.sh get clients -r master  --fields clientId)
  echo "clients are $clients ."

  if  [[ ! $clients = *"${MY_CLIENT}"* ]] ; then
    echo Client "$MY_CLIENT" will be added
    ./opt/jboss/keycloak/bin/kcadm.sh create clients -r master -s clientId="$MY_CLIENT" -s enabled=true -s clientAuthenticatorType=client-secret -s secret="$MY_SECRET" -s directAccessGrantsEnabled=true
  else
    echo The configured client "$MY_CLIENT" already exists
  fi
}

#echo secret "$MY_SECRET" client "$MY_CLIENT"

addClientIfNotPresent &

#start keycloak
/opt/jboss/tools/docker-entrypoint.sh -b 0.0.0.0
