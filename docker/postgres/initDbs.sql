CREATE DATABASE tenant;
CREATE USER patientdata_tenant WITH ENCRYPTED PASSWORD 'patientdata_tenant_pw';
GRANT ALL PRIVILEGES ON DATABASE tenant TO patientdata_tenant;

CREATE DATABASE patientdata;
CREATE USER patientdata_user WITH ENCRYPTED PASSWORD 'patientdata_pw';
GRANT ALL PRIVILEGES ON DATABASE patientdata TO patientdata_user;

CREATE DATABASE keycloak;
CREATE USER patientdata_keycloak WITH ENCRYPTED PASSWORD 'patientdata_keycloak_pw';
GRANT ALL PRIVILEGES ON DATABASE keycloak TO patientdata_keycloak;
