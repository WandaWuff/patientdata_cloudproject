# patientdata_cloudproject

Our Project for Cloud Application Development\
[Front end](https://gitlab.com/WandaWuff/patientdata_front_end)


# Links to shared documents:

[Specification](https://docs.google.com/document/d/1aLetHnOzSHFB43F1uqJgIilwkP-ZCE1VkDNqEME9E6M/edit?usp=sharing)\
[Functionality and requirements](https://docs.google.com/document/d/1kF278I1aQP0YL2ZL5v6vXOMTaQnZaGWV86GFkraItzo/edit?usp=sharing)\
[Data specification](https://docs.google.com/document/d/1JEih2Qs0b6IFGfBvvpuDds3Hsa-9F4OX8iwjVq5dCeg/edit)\
[Final documentation](https://docs.google.com/document/d/1EL0tT-ESBUmqCEf8O7p_T8vPTiQCUzWeJ41qwAiV4M0/edit?ts=5efb45d8)